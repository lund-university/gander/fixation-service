UNDEF       = -1000000
NORMAL      = 0
ACCUM       = 1
AREA        = 2
AREA_ACCUM  = 3

GES_NORMAL    = 0
GES_CENTER    = 1
GES_SIDE      = 2
GES_SEL_SIDE  = 3
GES_ANY_SIDE  = 4
GES_LABELS    = ["NONE", "CENTER", "SIDE", "SEL_SIDE", "ANY_SIDE"]

EPS = 0.001
CENTRAL_AREA = [0.5, 0.5]


class GazeGestures(object):
    # ======================================================    
    def __init__(self, minAreaDwell = 100, maxUndefTime = 100):
        # Global
        self.lastTstamp     = -1
        self.delaT          = 0
        # Detecting dwell on areas
        self.status         = NORMAL
        self.minAreaDwell   = minAreaDwell
        self.areaX          = UNDEF
        self.areaY          = UNDEF
        # Detecting gestures
        self.gesStatus      = GES_NORMAL
        self.side           = [UNDEF, UNDEF]
        self.sideTimer      = 0
        self.minSelectionDwell = 300
        self.maxSideDwell   = 1000
        self.gesture        = False
        self.gestureSide    = [UNDEF, UNDEF]
        self.undefCount     = 0
        self.maxUndefCount  = 2
    # ======================================================    
    def equal(self, a, b):
        return (abs(a[0]-b[0]) <= EPS) and (abs(a[1]-b[1]) <= EPS)
        
    # ======================================================    
    def isDef(self, area):
        return not self.equal(area, [UNDEF, UNDEF])

    # ======================================================    
    def update(self, tstamp, x, y):
        if self.lastTstamp != -1:
            self.deltaT = tstamp - self.lastTstamp
        self.gesture = False
        self.updateArea(tstamp, x, y)
        # print self.getCurrentArea()
        self.updateGestures(tstamp, x, y)
        self.lastTstamp = tstamp        
            
    # ======================================================    
    def getGesture(self):
        if self.gesture:
            return True, self.gestureSide, self.gesStatus
        return False, [UNDEF, UNDEF], self.gesStatus

    # ======================================================    
    def updateArea(self, tstamp, x, y):
        # Statechart
        # ----------- NORMAL ------------
        if self.status == NORMAL:
            if not self.isDef([x, y]):
                pass
            else:
                self.areaX, self.areaY = x, y
                self.status     = ACCUM
                self.timer      = 0
                self.undefCount = 0
        # ----------- ACCUM ------------                
        elif self.status == ACCUM:
            if not self.isDef((x, y)):
                self.undefCount += 1
                if self.undefCount > self.maxUndefCount:
                    self.status = NORMAL
                else:
                    x, y = self.areaX, self.areaY
            else:
                self.undefCount = 0
                
            if self.equal([self.areaX, self.areaY], [x, y]):
                self.timer += self.deltaT
                if self.timer >= self.minAreaDwell:
                    self.status = AREA
                    self.undefCount = 0
            else:
                self.areaX, self.areaY = x, y
                self.timer = 0

        # ----------- AREA ------------
        elif self.status == AREA:
            if not self.isDef([x, y]):
                self.undefCount += 1
                if self.undefCount > self.maxUndefCount:
                    self.status = NORMAL
                else:
                    x, y = self.areaX, self.areaY
            else:
                self.undefCount = 0

            if not self.equal([self.areaX, self.areaY], [x, y]):
                self.status = AREA_ACCUM
                self.undefCount = 0
                self.accum_areaX, self.accum_areaY = self.areaX, self.areaY # Remembering the last area
                self.areaX, self.areaY = x, y # Accumulate the new area
                self.timer  = 0                
        
        # ----------- AREA_ACCUM ------
        elif self.status == AREA_ACCUM:
            if not self.isDef([x, y]):
                self.undefCount += 1
                if self.undefCount > self.maxUndefCount:
                    self.status = NORMAL
                else:
                    x, y = self.areaX, self.areaY

            if self.equal([self.areaX, self.areaY], [x, y]):
                self.timer += self.deltaT
                if self.timer >= self.minAreaDwell:
                    self.status = AREA
                    self.undefCount = 0
            else:
                self.areaX, self.areaY = x, y
                self.timer = 0
                self.undefCount = 0

    # ======================================================    
    def updateGestures(self, tstamp, x, y):
        # Defining statechart
        currentArea = self.getCurrentArea()
        # ----------- NONE -----------------------------
        if self.gesStatus == GES_NORMAL:
            if self.isDef(currentArea):
                if self.equal(currentArea, CENTRAL_AREA): # Looking at the center
                    self.gesStatus = GES_CENTER
                else:
                    self.gesStatus = GES_ANY_SIDE
                
        # ----------- CENTER ---------------------------
        elif self.gesStatus == GES_CENTER:
            if self.isDef(currentArea):
                if not self.equal(currentArea, CENTRAL_AREA): # Looking to one of the areas
                    self.side = currentArea
                    self.gesStatus = GES_SIDE
                    self.sideTimer = 0
                else: 
                    pass # Still looking at the center, keep
            else:
                self.gesStatus = GES_NORMAL

        # ----------- SIDE -----------------------------
        elif self.gesStatus == GES_SIDE:
            if self.isDef(currentArea):
                if self.equal(self.side, currentArea): # Looking at the same side
                    self.sideTimer += self.deltaT
                    if self.sideTimer >= self.minSelectionDwell:
                        self.gesStatus = GES_SEL_SIDE
                else:
                    if self.equal(currentArea, CENTRAL_AREA): # Looking at the center
                        self.gesStatus = GES_CENTER
                    else:
                        self.gesStatus = GES_ANY_SIDE
            else:
                self.gesStatus = GES_NORMAL

        # ----------- SEL_SIDE -----------------------------
        elif self.gesStatus == GES_SEL_SIDE:
            if self.isDef(currentArea):
                if self.equal(self.side, currentArea): # Looking at the same side
                    self.sideTimer += self.deltaT
                    if self.sideTimer >= self.maxSideDwell: # Timeout with no selection
                        self.gesStatus = GES_ANY_SIDE
                else:
                    if self.equal(currentArea, CENTRAL_AREA): # Looking back at the center
                        # print "Gesture detected to side: ", self.side
                        self.gesture = True
                        self.gestureSide[0] = self.side[0]; self.gestureSide[1] = self.side[1]
                        self.gesStatus = GES_CENTER
                    else:
                        # Looking at another area
                        self.gesStatus = GES_ANY_SIDE
            else:
                self.gesStatus = GES_NORMAL

        # ----------- ANY_SIDE ---------------------------
        elif self.gesStatus == GES_ANY_SIDE:
            if self.isDef(currentArea):
                if self.equal(currentArea, CENTRAL_AREA): # Looking at the center
                    self.gesStatus = GES_CENTER
                else: 
                    pass # Still looking at any side, stays
            else:
                self.gesStatus = GES_NORMAL
        
    # ======================================================    
    def getCurrentArea(self):
        if self.status == AREA:
            return [self.areaX, self.areaY]
        elif self.status == AREA_ACCUM:
            return [self.accum_areaX, self.accum_areaY]
        return [UNDEF, UNDEF]
            
        
        
