# -*- coding: utf-8 -*-

from collections import deque
import numpy as np, sys
from gazeEventsDetection.constants import *
from gazeEventsDetection.GazeSample import *

from scipy.stats import norm, normaltest

class NormalDist(object):
    def __init__(self, coef):
        self.values     = deque(maxlen=NOISE_LEVEL_LEN)
        self.mean       = 0
        self.std        = 0
        self.threshold  = MIN_VEL_TH
        self.coef       = coef
        
    def addSample(self, v):
        """Add a new sample and recompute normal and standard deviation"""
        self.values.append(v)
        if len(self.values) == NOISE_LEVEL_LEN:
            # Estimating parameters from a normal distribution
            if True:
                self.mean, self.std = norm.fit(list(self.values))
            else:
                self.mean = np.mean(list(self.values))
                self.std  = np.std(list(self.values), ddof=1)
            th = max(self.mean + self.coef*self.std, MIN_VEL_TH)
            self.threshold = min(th, MAX_VEL_TH)
        else:
            self.threshold = MIN_VEL_TH # Chutando valor inicial
        return self.threshold
        
    def getThreshold(self):
        return self.threshold    
    
if __name__ == "__main__":
    # Antonio, detectar eventos do olhar
    filter = NormalDist(coef=4)
    import matplotlib.pyplot as plt
    V = np.random.normal(size=500)
    for i, v in enumerate(V):
        filter.addSample(v)
        plt.plot(i, filter.getThreshold(), "r.")
        plt.plot(i, v, 'b.')
        plt.plot(i, filter.mean, "g.")
    plt.show()
        
        
