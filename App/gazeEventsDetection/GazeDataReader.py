import csv
import json
from math import isnan
from gazeEventsDetection.GazeSample import GazeSample 

class GazeDataReader(object):
    def __init__(self, filePath="", header=False, divFactor=1000000):
        if filePath != "":
            #self.openFile(filePath, header)
            #self.openJson(filePath)
            self.eof = False
            self.open_tsv(filePath)
        self.c = 0
        self.divFactor = divFactor
        self.firstSample = True

    def openJson(self, filePath):
        try:
            self.file = open(filePath, "rt")
            data = json.load(self.file)
            self.jsonIter = iter(data)
        except:
            print('Error while parsing')
            self.file.close()
            return -1

    def getNextSampleJson(self):
        try:
            etData  = next(self.jsonIter, 'eof')
            if etData != 'eof':
                tstamp  = float(etData.get("ts"))
                rx      = float(etData.get("rx"))
                lx      = float(etData.get("lx"))
                ry      = float(etData.get("ry"))
                ly      = float(etData.get("ly"))
                pr      = float(etData.get("pr"))
                pl      = float(etData.get("pl"))
                s = GazeSample(tstamp, lx, ly, pl, rx, ry, pr)
                return s
            else:
                print('eof, no more jsonData to be read')
                return -1
        except:
            print('Error while parsing jsonData')
            return -1

    
    def openFile(self, filePath, header=False):
        try:
            self.file = open(filePath, "rt")
            self.reader = csv.reader(self.file)
            if header:
                next(self.reader)
            self.eof = False
            self.firstSample = True
        except:
            print("Unable to open file %s, no eye data file will be loaded"%(filePath))
            self.eof = True

    def getNextSample(self):
        if not self.eof:
            try:
                d = next(self.reader)
                tstamp, leftx, lefty = int(float(d[0]))/self.divFactor, int(float(d[1])), int(float(d[2]))
                if self.firstSample:
                    self.initTstamp = tstamp
                    print("Initial time stamp = ", tstamp)
                    self.firstSample = False
                tstamp = tstamp - self.initTstamp
                leftpupil = float(d[3])
                rightx, righty = int(float(d[4])), int(float(d[5]))
                rightpupil = float(d[6])
                s = GazeSample(tstamp, leftx, lefty, leftpupil, rightx, righty, rightpupil)
            except:
                s = -1
                self.eof = True
        else:
            s = -1
        return s


#####TObii sample to sample#####
    def tobii2Sample(self, input):
                #input is jsonObject of single sample
        #This method is supposed to create a single Sample object from this json object.
        x_pixels = 1920
        y_pixels = 1200
        try:
            data = float(input[('left_gaze_point_on_display_area_y')][0])
            if isnan(data):
                leftY = 0
            else:
                leftY =  data *y_pixels   #ly
        except Exception as e:
            print(f'Error: {e}\n{input[("left_gaze_point_on_display_area_y")]}')
            leftY = 0
        #####
        try:
            data = float(input[('left_gaze_point_on_display_area_x')][0])
            if isnan(data):
                leftX = 0
            else:
                leftX =  data  * x_pixels #lx
        except Exception as e:
            print(f'Error: {e}\n{input[("left_gaze_point_on_display_area_x")]}')
            leftX = 0
        ######
        try:
            data = float(input[('left_pupil_diameter')][0])
            if isnan(data):
                leftP = 0
            else:
                leftP = data   #lp
        except Exception as e:
            print(f'Error: {e}\n{input[("left_pupil_diameter")]}')
            leftP = 0
        ######
        try:
            data = float(input[('right_gaze_point_on_display_area_y')][0])
            if isnan(data):
                rightY = 0
            else:
                rightY = data  * y_pixels  #ry
        except Exception as e:
            print(f'Error: {e}\n{input[("right_gaze_point_on_display_area_y")]}')
            rightY = 0
        ######
        try:
            data = float(input[('right_gaze_point_on_display_area_x')][0])
            if isnan(data):
                rightX = 0
            else:
                rightX = data  * x_pixels   #rx
        except Exception as e:
            print(f'Error: {e}\n{input[("right_gaze_point_on_display_area_x")]}')
            rightX = 0
        ######
        try:
            data = float(input[('right_pupil_diameter')][0])
            if isnan(data):
                rightP = 0
            else:
                rightP =  data #rp
        except Exception as e:
            print(f'Error: {e}\n{input[("right_pupil_diameter")]}')
            rightP = 0
        ######
        try:
            data = int(input[('device_time_stamp')][0])
            if isnan(data):
                tstamp = 0
            else:
                tstamp = data /self.divFactor#ts
        except Exception as e:
            print(f'Error: {e}\n{input[("device_time_stamp")]}\ninput: {input}')
            tstamp = 0
            print('tstapms not found')
        if self.firstSample:
            self.initTstamp = tstamp
            self.firstSample = False
        tstamp = tstamp - self.initTstamp
        s = GazeSample(tstamp, leftX, leftY, leftP, rightX, rightY, rightP)
        return s
##### Json to Sample ###
    def json2Sample(self, input):
        #input is jsonObject of single sample
        #This method is supposed to create a single Sample object from this json object.
        jsonDecoder = json.JSONDecoder()
        
        json_in = jsonDecoder.decode(input)
        x_pixels = 1920
        y_pixels = 1200
        try:
            data = float(json_in.get('left_gaze_point_on_display_area_y'))
            if isnan(data):
                leftY = 0
            else:
                leftY =   data*y_pixels   #ly
        except:
            leftY = 0
        ######
        try:
            data = float(json_in.get('left_gaze_point_on_display_area_x'))
            if isnan(data):
                leftX = 0
            else:
                leftX =    data* x_pixels #lx
        except:
            leftX = 0
        #######
        try:
            data = float(json_in.get('left_pupil_diameter'))
            if isnan(data):
                leftP = 0
            else:
                leftP = data #lp
        except:
            leftP = 0
        ######
        try:
            data = float(json_in.get('right_gaze_point_on_display_area_y'))
            if isnan(data):
                rightY = 0
            else:
                rightY =  data * y_pixels  #ry
        except:
            rightY = 0
        ######
        try:
            data = float(json_in.get('right_gaze_point_on_display_area_x'))
            if isnan(data):
                rightX = 0
            else:
                rightX =  data * x_pixels   #rx
        except:
            rightX = 0
        ######
        try:
            data = float(json_in.get('right_pupil_diameter'))
            if isnan(data):
                rightP = 0
            else:
                rightP =  data #rp
        except:
            rightP = 0
        ######
        tstamp =  int(json_in.get('device_time_stamp'))/self.divFactor#ts
        if self.firstSample:
            self.initTstamp = tstamp
            self.firstSample = False
        tstamp = tstamp - self.initTstamp
        s = GazeSample(tstamp, leftX, leftY, leftP, rightX, rightY, rightP)
        return s

#####################################READING THE TSV FILE WE GET AS OUTPUT FROM WEBSOCKET######################################################
    def open_tsv(self, filePath, header=True):
        try:
            self.file = open(filePath)
            print("open")
            self.reader = csv.reader(self.file)
            if header:
                self.header = next(self.reader)
                #totalt 44 kolumner, här definierar vi vilket index var och ett av attributen är på.
                self.indexLeftY = 5     #'left_gaze_point_on_display_area_y'
                self.indexLeftX = 4     #'left_gaze_point_on_display_area_x'
                self.indexLeftP = 19    #'left_pupil_diameter'
                self.indexRightY = 26   #'right_gaze_point_on_display_area_y'
                self.indexRightX = 25   #'right_gaze_point_on_display_area_x'
                self.indexRightP = 40   #'right_pupil_diameter'
                self.indexTstamp = 0    #device_time_stamp
            self.eof = False
            self.firstSample = True
        except:
            print('Error while reading tsv file')

    def getNextSampleTsv(self):
        if not self.eof:
            try:
                d = next(self.reader)
                d = d[0].split("\t")
                
                #device time stamp as an int, divided by 1000 to get it in milliseconds
                tstamp = int(float(d[self.indexTstamp])/self.divFactor)

                if self.firstSample:
                    self.initTstamp = tstamp
                    print("initial time stamp = ", tstamp)
                    self.firstSample = False
                #x and y coordinates of left eye , scaled by the pixels of the screen
                leftx, lefty = float(d[self.indexLeftX])*1920, float(d[self.indexLeftY])*1200
                #x and y coordinates of right eye , scaled by the pixels of the screen
                rightx, righty = float(d[self.indexRightX])*1920, float(d[self.indexRightY])*1200
                #Do nan-check as to not crash everythong.

                if isnan(leftx) and isnan(lefty):
                    leftx, lefty = 0, 0
                elif isnan(leftx):
                    leftx = lefty
                elif isnan(lefty):
                    lefty = leftx

                if isnan(rightx) and isnan(righty):
                    rightx, righty = 0, 0
                elif isnan(rightx):
                    rightx = righty
                elif isnan(righty):
                    righty = rightx

                tstamp = tstamp - self.initTstamp

                leftpupil = float(d[self.indexLeftP])
                rightpupil = float(d[self.indexRightP])
                
                if isnan(leftpupil):
                    leftpupil = 0

                if isnan(rightpupil):
                    rightpupil = 0

                s = GazeSample(tstamp, leftx, lefty, leftpupil, rightx, righty, rightpupil)
            except:
                s = -1
                self.eof
                self.file.close()
        else:
            s = -1
        return s