import os
from pathlib import Path
import tkinter as tk
from tkinter import filedialog
from log_analysis import *
from plot_log_analysis import *

def move_figure(fig, 
                x=0, 
                y=0, 
                width=800, 
                height=600, 
                title=None, 
                title_bar_offset=30):

    manager = fig.canvas.manager
    
    adjusted_y = y + title_bar_offset

    try:
        manager.window.setGeometry(x, adjusted_y, width, height)
    except Exception:
        pass

    if title is not None:
        try:
            manager.window.setWindowTitle(title)
        except Exception:
            try:
                manager.set_window_title(title)
            except Exception:
                pass


def move_figures_in_grid(figs, rows=4, cols=4,
                         screen_width=1920, screen_height=1080):
    cell_width = screen_width // cols
    cell_height = screen_height // rows

    for i, fig in enumerate(figs):
        row = i // cols
        col = i % cols

        x = col * cell_width
        y = row * cell_height

        title = f"Figure {i+1}"
        
        move_figure(fig,
                    x=x,
                    y=y,
                    width=cell_width,
                    height=cell_height,
                    title=title,
                    title_bar_offset=30)

def select_log_file():
    cwd = os.path.abspath(os.getcwd())
    cwd = os.path.split(cwd)[0]
    logs_path = os.path.join(cwd, 'demo', 'logs')
    files = [f for f in os.listdir(logs_path) if os.path.isfile(os.path.join(logs_path, f))]

    print("Available log files:")
    for i, file in enumerate(files):
        print(f"{i + 1}. {file}")

    choice = int(input("Enter the number of the file you want to process: "))
    selected_file = files[choice - 1] 

    return selected_file

if __name__ == '__main__':
    selected_file = select_log_file()

    cwd = os.path.abspath(os.getcwd())
    cwd = os.path.split(cwd)[0] 
    logs_path = os.path.join(cwd, 'demo', 'logs') 
    selected_file_path = Path(logs_path) / selected_file

    files = [selected_file_path]

    # Maps with log file as key
    fixations = {}
    show_more_events = {}
    parser_response = {}
    parser_response_full = {}
    file_patches = {}
    diff_lines = {}

    # Populate maps with various data from the log files
    populate_maps(files, fixations, show_more_events, parser_response, parser_response_full, file_patches, diff_lines)

    # Process the data collected from log files and save to H5 file
    save_data_to_file(code_base_metrics(parser_response), new=True)
    save_data_to_file(src_vs_target(fixations))
    save_data_to_file(loc_over_time(fixations, show_more_events, file_patches, get_file_lengths(parser_response)))
    save_data_to_file(aoi_over_time(fixations))
    save_data_to_file(code_coverage(fixations, parser_response, diff_lines))
    save_data_to_file(code_coverage_visibility(fixations, parser_response, diff_lines, file_patches, show_more_events))
    save_data_to_file(line_coverage(fixations, parser_response))
    save_data_to_file(code_coverage_over_time(fixations, parser_response, diff_lines, file_patches, show_more_events))

    # Plot data from H5 file
    with h5py.File('./data.h5', 'r') as hdf:
        figs = []

        figs.extend(total_fixations_per_aoi(hdf, n=8))
        figs.extend(scarf_plot(hdf, n=8))
        figs.extend(plot_loc_over_time(hdf))
        figs.extend(plot_code_coverage_over_time(hdf))

        screen_width = 1920
        screen_height = 1080

        first_row_height = 350
        first_row_midpoint = 768
        second_row_midpoint = 960

        move_figure(figs[0], 0, 0, first_row_midpoint, first_row_height)
        move_figure(figs[1], first_row_midpoint, 0, screen_width - first_row_midpoint, first_row_height)
        move_figure(figs[2], 0, first_row_height, second_row_midpoint, screen_height - first_row_height, title_bar_offset=60)
        move_figure(figs[3], second_row_midpoint, first_row_height, second_row_midpoint, screen_height - first_row_height, title_bar_offset=60)

        plt.show()