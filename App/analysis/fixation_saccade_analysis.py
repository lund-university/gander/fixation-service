import numpy as np
import os
from pathlib import Path
import matplotlib.pyplot as plt
import pandas as pd

def calculate_saccade_length(x1, y1, x2, y2):
    return np.sqrt((x2 - x1)**2 + (y2 - y1)**2)

if __name__ == '__main__':
    
    cwd = os.path.abspath(os.getcwd())
    cwd = os.path.split(cwd)[0]
    cwd = os.path.join(cwd, 'App')
    cwd = os.path.join(cwd, 'output')

    path = Path(cwd)

    data = pd.read_csv(path.name + '/allfixations.txt', sep="\t")

    trials = data.groupby('participant')

    data['saccade_length'] = calculate_saccade_length(data['xpos'], data['ypos'], data['xpos'].shift(-1), data['ypos'].shift(-1))

    for trial_name, trial_data in trials:
        avg_saccade_length = trial_data['saccade_length'].mean()
        avg_fixation_duration = trial_data['dur'].mean()
        print(f"{trial_name}: Average Fixation Duration: {avg_fixation_duration}, Average Saccade Length: {avg_saccade_length}")
