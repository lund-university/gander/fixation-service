import numpy as np
import re
import sys

# Returns a set of all line numbers whose corresponding line contains either additions or deletions
def get_diff_lines_for_patch(patch, additions):
    symbol = '+' if additions else '-'

    lines = set()
    start_src_line = patch.get('startSrcLine')

    current_line = start_src_line
    
    for part in patch.get('codeParts', []):
        startsWith = part.get('startsWith')
        numberOfLines = part.get('nbrOfLines')
        if startsWith == symbol:
            for i in range(numberOfLines):
                lines.add(current_line + i)

        if startsWith == symbol or startsWith == ' ':
            current_line += numberOfLines
            
    return lines

# Exatract type and start information
def extract_aoi_details(element_id):
    match = re.search(r'aoi-(.*)-parent:.*-(\d+):(\d+)-file:', element_id)
    if match:
        aoi_type = match.group(1)
        start_line = match.group(2)
        start_column = match.group(3)
        return aoi_type, f"{start_line}:{start_column}"
    return None, None

# Recursively collects all children in a list
def collect_all_descendants(ast, depth):
    descendants = []

    if 'children' in ast and depth > 0:
        for child in ast.get('children'):
            descendants.append(child.get('type'))
            descendants.extend(collect_all_descendants(child, depth - 1))

    return descendants

# Find target aoi in AST and return descendants
def find_aoi_and_collect_children(ast, target_type, target_start, depth):
    if ast.get('type') == target_type and ast.get('start') == target_start:
        if depth == None:
            return collect_all_descendants(ast, sys.maxsize * 2 + 1)
        else:
            return collect_all_descendants(ast, depth)
    
    if 'children' in ast:
        for child in ast.get('children'):
            result = find_aoi_and_collect_children(child, target_type, target_start, depth)
            if result is not None and result != []:
                return result
            
    return []

# Get descendants from ast based on aoi in fixation
def get_aoi_children_recursive(fixation, ast, depth = None):
    element_id = fixation.get('element-id')
    aoi_type, start_position = extract_aoi_details(element_id)
    return find_aoi_and_collect_children(ast, aoi_type, start_position, depth)

# Get children of a specific aoi type
def get_children_of_aoi_type_recursive(ast, type):
    aois = []

    if ast.get('type') == type:
        for child in ast.get('children'):
            aois.append(child.get('type'))

    if 'children' in ast:
        for child in ast.get('children'):
            aois += get_children_of_aoi_type_recursive(child, type) 

    return aois

# Returns a set of AOIs in the AST
def recursive_aoi_set(ast):
    aois = set()

    start = ast.get('start').split(':')
    end= ast.get('end').split(':')
    aoi_type = ast.get('type')

    identifier = f"{start}:{end}:{aoi_type}"

    # TODO: This line and others in similar methods below might not work for every language, it
    # depends on the Tree-Sitter grammar.
    if not (start[0] != end[0] and not 'statement' in aoi_type and not 'lambda' in aoi_type and not 'block_comment' in aoi_type):
        aois.add(identifier)
    
    if 'children' in ast:
        for child in ast.get('children'):
            aois.update(recursive_aoi_set(child))

    return aois

# Returns a list of all AOI types in the AST
def recursive_aoi_list(ast):
    aois = []

    start = ast.get('start').split(':')
    end= ast.get('end').split(':')
    aoi_type = ast.get('type')

    if not (start[0] != end[0] and not 'statement' in aoi_type and not 'lambda' in aoi_type and not 'block_comment' in aoi_type):
        aois.append(aoi_type)
    
    if 'children' in ast:
        for child in ast.get('children'):
            aois.extend(recursive_aoi_list(child))

    return aois

# Recursive function to count AOIs in the AST
def recursive_aoi_list_in_lines(ast, lines):
    aois = []

    line_start = ast.get('start').split(':')[0]
    line_end= ast.get('end').split(':')[0]
    aoi_type = ast.get('type')

    if not (line_start != line_end and not 'statement' in ast.get('type') and not 'lambda' in ast.get('type') and not 'block_comment' in ast.get('type')):
        if int(line_start) in lines:
            aois.append(aoi_type)
    
    if 'children' in ast:
        for child in ast.get('children'):
            aois.extend(recursive_aoi_list_in_lines(child, lines))

    return aois

#recursive function to count AOIs in the AST only on specific lines
def recursive_aoi_set_in_lines(ast, lines):
    aois = set()

    start = ast.get('start').split(':')
    end = ast.get('end').split(':')
    aoi_type = ast.get('type')

    identifier = f"{start}:{end}:{aoi_type}"

    if not (start[0] != end[0] and not 'statement' in aoi_type and not 'lambda' in aoi_type and not 'block_comment' in aoi_type):
        if int(start[0]) in lines:
            aois.add(identifier)
    
    if 'children' in ast:
        for child in ast.get('children'):
            aois.update(recursive_aoi_set_in_lines(child, lines))

    return aois

# Recursive function to count AOIs in the AST
def recursive_aoi_count_in_lines(ast, lines):
    res=0
    line_start = ast.get('start').split(':')[0]
    line_end= ast.get('end').split(':')[0]
    if not (line_start != line_end and not 'statement' in ast.get('type') and not 'lambda' in ast.get('type') and not 'block_comment' in ast.get('type')):
        if int(line_start) in lines:
            res+=1
    
    if 'children' in ast:
        for child in ast.get('children'):
            res+=recursive_aoi_count_in_lines(child, lines)
    return res

# Recursive function to count AOIs in the AST
def recursive_aoi_count(ast):
    res=0
    line_start = ast.get('start').split(':')[0]
    line_end= ast.get('end').split(':')[0]
    if not (line_start != line_end and not 'statement' in ast.get('type') and not 'lambda' in ast.get('type') and not 'block_comment' in ast.get('type')):
        res+=1
    
    if 'children' in ast:
        for child in ast.get('children'):
            res+=recursive_aoi_count(child)
    return res
    
# Recursive function to count AOIs in the AST between specific lines
def recursive_aoi_set_between_lines(ast, from_line, to_line):
    aois = set()

    start = ast.get('start').split(':')
    end = ast.get('end').split(':')
    aoi_type = ast.get('type')

    identifier = f"{start}:{end}:{aoi_type}"
    to_line = sys.maxsize * 2 + 1 if to_line == float('inf') else to_line
    if int(start[0]) >= from_line and int(start[0]) <= to_line:
        if not (start[0] != end[0] and not 'statement' in aoi_type and not 'lambda' in aoi_type and not 'block_comment' in aoi_type):
            aois.add(identifier)
    
    if 'children' in ast:
        for child in ast.get('children'):
            aois.update(recursive_aoi_set_between_lines(child, from_line, to_line))

    return aois

# Recursive function to count visible AOIs in the AST for each visiblity period
def recursive_aoi_set_periods(key, file_name, ast, visibility_periods):
    aoi_set = {}
    total = set()

    fromTime = -1
    for patch in visibility_periods[key][file_name]:
        if patch['from'] == fromTime:
            aoi_set[fromTime].update(recursive_aoi_set_between_lines(ast, patch['start'], patch['end']))
        else:
            fromTime = patch['from']
            total = total.union(recursive_aoi_set_between_lines(ast, patch['start'], patch['end']))
            
            aoi_set[fromTime] = total

    return aoi_set

# Returns a dictionary that maps from orginal line numbers to line numbers with additions and deletions removed
def get_diff_adjusted_lines(diff_lines, file_lengths):
    adjusted_lines = {}

    for file_name in diff_lines:
        length = file_lengths[file_name]

        i = 1 
        line_number = 1

        adjusted_lines[file_name] = {}

        while i < length + 1:
            if i not in diff_lines[file_name]:
                adjusted_lines[file_name][i] = line_number
                line_number += 1

            i += 1

    return adjusted_lines

# Returns the line number in element_id of a fixation
def extract_line_number(element_id):
    match = re.search(r'-(\d+):\d+-', element_id)
    if match:
        line_number = int(match.group(1))
        return line_number
    return None

# Replaces line number in element_id of a fixation
def replace_line_number(element_id, new_line_number):
    pattern = r'-(\d+):(\d+)-'
    replacement = f'-{new_line_number}:\\2-'
    new_element_id = re.sub(pattern, replacement, element_id)
    return new_element_id

# Returns total amoint of visible aois for time 
def get_aoi_count_for_time(periods, time):
    count = 0
    for period in periods:
        if time >= period:
            count = len(periods[period])

    return count 

# Maps the the element-id of fixations to the size details of the corresponding AOI found in parser_response
def map_aoi_to_size(parser_response):
    aoi_to_size = {}
    visited_files = set()

    for key in parser_response:
        for f in parser_response[key]:
            ast = f.get('Response')
            file_name = f.get('FileName')

            if file_name not in visited_files:
                visited_files.add(file_name)

                recursive_map_aoi_to_size(aoi_to_size, ast, file_name)

    return aoi_to_size

# Recursively populates the map with size details of AOIs
def recursive_map_aoi_to_size(mapping, ast, file_name):
    start = ast.get('start')
    end = ast.get('end')

    start_line, start_col = map(int, start.split(':'))
    end_line, end_col = map(int, end.split(':'))

    base_line_size = 100  
    line_diff = end_line - start_line
    col_diff = end_col - start_col

    size = line_diff * base_line_size + col_diff

    identifier = f"aoi-{ast.get('type')}-{start}-file:{file_name}" 
    mapping[identifier] = (start_line, start_col, end_line, end_col, size)
    
    if 'children' in ast:
        for child in ast.get('children'):
            recursive_map_aoi_to_size(mapping, child, file_name)

# For each key and file, returns the set of all lines that were visible at some point
def get_visible_lines(visibility_periods, file_lengths):
    visible_lines = {}

    for log_file_name, files in visibility_periods.items():
        if log_file_name not in visible_lines:
            visible_lines[log_file_name] = {}

        for code_file_name, periods in files.items():
            if code_file_name not in visible_lines[log_file_name]:
                visible_lines[log_file_name][code_file_name] = set()

            for period in periods:
                end_line = period['end']
                if end_line == float('inf'):
                    end_line = file_lengths.get(code_file_name, 0)
                visible_lines[log_file_name][code_file_name].update(range(period['start'], end_line + 1))

    return visible_lines

# Returns the set of visible aois for each visibility period
def calculate_aoi_set_per_visibility_period(parser_response, visibility_periods, diff_lines):
    aois = {}

    for key in parser_response:
        aois[key] = {}
        for f in parser_response[key]:
            ast = f.get('Response')
            file_name = f.get('FileName')

            if '-fileview' not in file_name:

                if '-target' in file_name:
                    aois_in_file = recursive_aoi_set_periods(key, file_name, ast, visibility_periods)
                elif '-src' in file_name:
                    aois_in_lines = recursive_aoi_set_in_lines(ast, diff_lines[file_name])
                    for period in visibility_periods[key][file_name]:
                        aois_in_file[period['from']] = aois_in_lines

                file_name = file_name.replace('-src', '').replace('-target', '')

                if file_name in aois[key]:
                    for period in aois[key][file_name]:
                        aois[key][file_name][period].update(aois_in_file[period])
                else:
                    aois[key][file_name] = {}

                    for period in aois_in_file:
                        aois[key][file_name][period] = aois_in_file[period]

    return aois

# Returns time ranges for line visibility
# TODO: Is this affected by Gerrit mode? 
# My initial thought is that it isn't, since "visible" means that is can be seen, not that it is 
# currently looked at. If the "show more" parts stay open when going back and forth to the overview
# we should be fine. 
def get_visibility_periods(file_patches, show_more_events):
    visibility_periods = {}
    
    for log_file_name in file_patches:
        if log_file_name not in visibility_periods:
            visibility_periods[log_file_name] = {}

        for code_file_name in file_patches[log_file_name]:

            if code_file_name not in visibility_periods[log_file_name]:
                visibility_periods[log_file_name][code_file_name] = []

            for patch in file_patches[log_file_name][code_file_name]:
                visibility_periods[log_file_name][code_file_name].append({'start': patch['start'], 'end': patch['end'], 'from': 0, 'to': sys.maxsize * 2 + 1})                    

    for log_file_name, events in show_more_events.items():
        if log_file_name in visibility_periods:
            events_sorted = sorted(events, key=lambda x: int(x['msSinceEpoch']))
            for i, event in enumerate(events_sorted):
                event_time = int(event['msSinceEpoch'])
                element_id_parts = event['element-id'].split('-')
                action_type = element_id_parts[-1]

                if action_type == 'top':
                    code_filename = element_id_parts[-2]
                    line_specifier = None
                else:
                    action_type = 'bottom'
                    code_filename = element_id_parts[-3]
                    line_specifier = int(element_id_parts[-1])
                
                if action_type == 'top':
                    newSrcVisiblity = {'start': 1, 'end': file_patches[log_file_name][code_filename + '-src'][0]['start'], 'from': event_time, 'to': None}
                    newTargetVisiblity = {'start': 1, 'end': file_patches[log_file_name][code_filename + '-target'][0]['start'], 'from': event_time, 'to': None}
                    
                elif action_type == 'bottom':
                    next_patch_start = float('inf')
                    for patch in file_patches[log_file_name][code_filename + '-target']:
                        if patch['start'] == line_specifier:
                            patchFrom = file_patches[log_file_name][code_filename + '-src'][file_patches[log_file_name][code_filename + '-target'].index(patch)]

                        if patch['start'] > line_specifier:
                            patchTo = file_patches[log_file_name][code_filename + '-src'][file_patches[log_file_name][code_filename + '-target'].index(patch)]

                            next_patch_start = patch['start']
                            break

                    newTargetVisiblity = {'start': line_specifier, 'end': next_patch_start, 'from': event_time, 'to': None}

                    if next_patch_start == float('inf'):
                        newSrcVisiblity = {'start': patchFrom['start'], 'end': next_patch_start, 'from': event_time, 'to': None}
                    else:
                        newSrcVisiblity = {'start': patchFrom['start'], 'end': patchTo['start'], 'from': event_time, 'to': None}

                if i == 0:
                    for period in visibility_periods[log_file_name][code_filename + '-src']:
                        if period['from'] == 0:
                            period['to'] = event_time
                    for period in visibility_periods[log_file_name][code_filename + '-target']:
                        if period['from'] == 0:
                            period['to'] = event_time   
                    
                if i < len(events_sorted) - 1:
                    next_event_time = int(events_sorted[i + 1]['msSinceEpoch'])
                    newSrcVisiblity['to'] = next_event_time
                    newTargetVisiblity['to'] = next_event_time
                else:
                    newSrcVisiblity['to'] = sys.maxsize * 2 + 1
                    newTargetVisiblity['to'] = sys.maxsize * 2 + 1

                visibility_periods[log_file_name][code_filename + '-src'].append(newSrcVisiblity)
                visibility_periods[log_file_name][code_filename + '-target'].append(newTargetVisiblity)

    return visibility_periods

# Datatype for storing visibility periods in H5 files
visibility_period_type = np.dtype([
    ('start', np.float64),
    ('end', np.float64),
    ('from', np.float64),
    ('to', np.float64)
])

# Returns the longest length of each file (src, target, fileview) from parser_response
def get_file_lengths_longest(parser_response):
    lengths = {}

    for key in parser_response:
        for f in parser_response[key]:
            file_name = f.get('FileName').replace('-src', '').replace('-target', '').replace('-fileview', '')

            length = int(f.get('Response')['end'].split(':')[0])

            if file_name in lengths:
                lengths[file_name] = max(lengths[file_name], length)
            else:
                lengths[file_name] = length

    return lengths

# Returns the length of each file (src, target, fileview) from parser_response
def get_file_lengths(parser_response):
    lengths = {}

    for key in parser_response:
        for f in parser_response[key]:
            file_name = f.get('FileName')
            length = int(f.get('Response')['end'].split(':')[0])
            lengths[file_name] = length

    return lengths
