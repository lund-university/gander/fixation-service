from plot_utility_functions import *
from log_code_coverage import *
from log_utility_functions import *
import numpy as np
import matplotlib.pyplot as plt
import h5py
from collections import Counter

def plot_loc_over_time(hdf):
    figs = []

    for key in hdf.keys():

        group_path = f'{key}/loc_over_time'

        if group_path not in hdf:
            continue # We move on if there are no loc metrics for the log file

        loc_group = hdf[group_path]
        files = [name for name in loc_group.keys() if name not in ('start_times', 'file_lengths')]

        file_lengths = {name: loc_group['file_lengths'][name][()] for name in loc_group['file_lengths']}
        ratios = [file_lengths.get(file, 0) for file in files]

        fig, axs = plt.subplots(
            ncols=1, 
            nrows=len(files),
            layout="constrained", 
            squeeze=False, 
            sharex=True, 
            sharey=False, 
            gridspec_kw={'height_ratios': ratios}
        )
        
        max_x = 0
        for n, file in enumerate(files):
            ax = axs[n][0]

            ax.set_facecolor('lightgray')
            ax.title.set_text(f'File: {file}')
            ax.set_xlabel('Time')
            ax.set_ylabel('Line')

            if file in loc_group:
                data = loc_group[file][()]
                if data.size > 0:
                    times, locs = zip(*data)
                    ax.plot(times, locs, color='blue')
                    max_x = max(max_x, max(times))
            
            ax.set_ylim([0, ratios[n]])
            ax.margins(x=0, y=0)
            ax.xaxis.set_tick_params(which='both', labelbottom=True)
            ax.yaxis.set_inverted(True)
        
        if 'visibility_periods' in hdf[f'{key}']:
            visibility_periods = hdf[f'{key}']['visibility_periods']
            for n, file in enumerate(files):
                ax = axs[n][0]

                for targetOrSrc in ['-target', '-src']:
                    file_name = file + targetOrSrc

                    if file_name in visibility_periods:
                        for period in visibility_periods[file_name]:
                            y1 = period['start'] - 1
                            y2 = period['end'] if not np.isinf(period['end']) else file_lengths.get(file_name, ratios[n])
                            start_time = loc_group['start_times'][file][()] if file in loc_group['start_times'] else 0
                            x1 = max(period['from'] - start_time, 0)
                            x2 = max_x
                            ax.fill_betweenx(
                                y=[y1, y2],
                                x1=x1,
                                x2=x2,
                                color='white',
                                alpha=1.0,
                                edgecolor='none'
                            )
                                
        fig.suptitle(f"Line of Code Over Time")
        figs.append(fig)

    return figs

def scarf_plot(hdf, n=6):
    color_palette = get_aoi_colors(n + 1, 'Set1')
    colors = {}

    filtered_keys = [key for key in hdf.keys() if key != 'code_base_metrics']

    fig, axs = plt.subplots(ncols=1, nrows=len(filtered_keys),
                            figsize=(8, 6), squeeze=False, sharex=True)
  
    for ax, key in zip(axs.flatten(), filtered_keys):
        group = hdf[key]
        tuples = group['aoi_over_time'][()]

        timestamps, aoi_types = zip(*tuples)

        aoi_types_counts = Counter(aoi_types)
        most_common_types = aoi_types_counts.most_common(n)
        aoi_types_sorted, counts_list = zip(*most_common_types)
        aoi_types_sorted = [x.decode('utf-8') for x in aoi_types_sorted]

        other_count = sum(count for aoi, count in aoi_types_counts.items() if aoi.decode('utf-8') not in aoi_types_sorted)

        aoi_types_sorted.append("miscellaneous")
        counts_list = list(counts_list) + [other_count]

        sorted_aoi_types_and_counts = sorted(zip(aoi_types_sorted, counts_list), key=lambda x: x[1], reverse=True)
        aoi_types_sorted, counts_list = zip(*sorted_aoi_types_and_counts)

        colors.update({aoi: color_palette[i] for i, aoi in enumerate(aoi_types_sorted)})

        for timestamp, action_type in tuples:
            action_type = action_type.decode('utf-8') if isinstance(action_type, bytes) else action_type

            color = colors.get(action_type, "blue") if action_type in aoi_types_sorted else "blue"

            ax.barh(y=key, width=20000, height=1, left=timestamp, color=color)

        xmin, xmax = ax.get_xlim()
        ax.set_xlim(0, max(xmax, max(timestamps)))
        ax.set_title(f"AOI Types Over Time")

    legend_handles = [plt.Rectangle((0, 0), 1, 1, color=colors.get(aoi_type, "blue")) 
                      for aoi_type in aoi_types_sorted] 
    legend_labels = list(aoi_types_sorted)

    plt.legend(legend_handles, legend_labels, loc='center left', bbox_to_anchor=(1.0, 0.5), title="Legend")
    plt.subplots_adjust(left=0.08, right=0.780, top=0.80, bottom=0.2)

    return [fig]

def total_fixations_per_aoi(hdf, n=6):
    color_palette = get_aoi_colors(n + 1, 'Set1') 
    colors = {}

    filtered_keys = [key for key in hdf.keys() if key != 'code_base_metrics']

    figures = []
    for key in filtered_keys:
        group = hdf[key]
        tuples = group['aoi_over_time'][()]

        timestamps, aoi_types = zip(*tuples)

        aoi_types_counts = Counter(aoi_types)
        most_common_types = aoi_types_counts.most_common(n)
        aoi_types_sorted, counts_list = zip(*most_common_types)
        aoi_types_sorted = [x.decode('utf-8') for x in aoi_types_sorted]

        other_count = sum(count for aoi, count in aoi_types_counts.items() if aoi.decode('utf-8') not in aoi_types_sorted)

        aoi_types_sorted.append("miscellaneous")
        counts_list = list(counts_list) + [other_count]

        sorted_aoi_types_and_counts = sorted(zip(aoi_types_sorted, counts_list), key=lambda x: x[1], reverse=True)
        aoi_types_sorted, counts_list = zip(*sorted_aoi_types_and_counts)

        colors.update({aoi: color_palette[i] for i, aoi in enumerate(aoi_types_sorted)})

        fig, ax = plt.subplots(figsize=(8, 4))

        bar_colors = [colors.get(aoi, "skyblue") for aoi in aoi_types_sorted]
        ax.bar(range(len(aoi_types_sorted)), counts_list, color=bar_colors)

        ax.set_xticks(range(len(aoi_types_sorted)))
        ax.set_xticklabels(aoi_types_sorted, rotation=45, ha='right')
        ax.set_title(f"AOI Types sorted by number of fixations")
        ax.set_xlabel("AOI Type")
        ax.set_ylabel("Fixation count")
        fig.tight_layout()

        figures.append(fig)

    return figures

def plot_code_coverage_over_time(hdf):
    filtered_keys = [key for key in hdf.keys() if key != 'code_base_metrics']
    figs = []

    for key in filtered_keys:
        group = hdf[key]['code_coverage_over_time']
        end_time = group['end_times'][()]
        
        fig, axs = plt.subplots(ncols=1, nrows=len(group) - 1,
                        layout="constrained", squeeze=False, sharex=True, sharey=True)

        n=0
        for file in group:
            if file == 'end_times':
                continue

            ax = axs[n][0]

            ax.set_facecolor('white')
            ax.title.set_text(f"File: {file}")
            ax.set_xlabel('Time')
            ax.set_ylabel('Code coverage')
            
            times, coverage = map(list, zip(*group[file][()]))

            times.append(end_time)
            coverage.append(coverage[-1]) 

            plot_steps_with_color(ax, times, coverage)

            ax.set_ylim(0, 1)
            ax.margins(x=0, y=0)

            ax.xaxis.set_tick_params(which='both', labelbottom=True)
         
            n += 1
        
        fig.suptitle(f"Code Coverage Over Time")
        figs.append(fig)

    return figs

if __name__ == '__main__':

    with h5py.File('./data.h5', 'r') as hdf:
        total_fixations_per_aoi(hdf)
        plot_loc_over_time(hdf)
        scarf_plot(hdf)
        plot_code_coverage_over_time(hdf)

        plt.show()
        