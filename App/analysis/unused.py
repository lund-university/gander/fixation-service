# LOC over time for each file and participant, end of file marked with dark grey
def loc_over_time_shaded(fixations, show_more_events, file_patches, file_lengths):
    visibility_periods = get_visibility_periods(file_patches, show_more_events)

    files = {}
    startTimes = {}
    for key in fixations:
        files[key] = {}
        startTimes[key] = {}
        startTime = 0
        prev_file = None
        for f in fixations[key]:
            file_name = f.get('element-id').split('-')[-2].split(':')[-1]
            file_line = int(f.get('element-id').split('-')[3].split(':')[0])

            if prev_file!= None and prev_file!=file_name:
                files[key][prev_file].append((time+1000, float("NaN")))

            time= int(f.get('msSinceEpoch'))
            if startTime == 0:
                startTime = time

            startTimes[key][file_name] = startTime

            time = time-startTime
            if file_name in files[key]:
                files[key][file_name].append((time,file_line))
            else:
                files[key][file_name]=[(time,file_line)]

            prev_file=file_name

    for key in files:
        fig, axs = plt.subplots(ncols=1, nrows=len(files[key]),
                        layout="constrained", squeeze=False, sharex=True, sharey=True)
        
        max_x = 0
        max_y = 0

        for n, file in enumerate(files[key]):
            ax = axs[n][0]

            ax.set_facecolor('gray')
            ax.title.set_text(file)
            ax.set_xlabel('Time')
            ax.set_ylabel('Line')
            
            loc=[]
            times=[]
            for i in files[key][file]:
                times.append(i[0])
                loc.append(i[1])
        
            ax.plot(times, loc)

            max_x = max(max_x, max(times))
            max_y = max(max_y, max(loc))

            ax.margins(x=0, y=0)
            ax.xaxis.set_tick_params(which='both', labelbottom=True)
            axGca = plt.gca()
            axGca.yaxis.set_inverted(True)         

        for n, file in enumerate(files[key]):
            ax = axs[n][0]

            for targetOrSrc in ['-target', '-src']:
                file_name = file + targetOrSrc

                if file_name in file_lengths:
                    ax.fill_betweenx(y=[1, file_lengths[file_name]], 
                                        x1=0, 
                                        x2=max_x, 
                                        color='lightgray', 
                                        alpha=1.0, 
                                        edgecolor='none')                
                        
            for targetOrSrc in ['-target', '-src']:
                file_name = file + targetOrSrc

                if key in visibility_periods:
                    for period in visibility_periods[key][file_name]:
                        y1 = period['start'] - 1
                        y2 = period['end'] if period['end'] != float('inf') else file_lengths[file_name]
                        startTime = startTimes[key][file]
                        x1 = max(period['from'] - startTime, 0)
                        x2 = max_x

                        ax.fill_betweenx(y=[y1, y2], 
                                            x1=x1, 
                                            x2=x2, 
                                            color='white', 
                                            alpha=1.0, 
                                            edgecolor='none')

        fig.suptitle(key)

    plt.show()

### bar plot, contents of expression_statements that are fixated on
def contents_of_expression_statements(fixations, parser_response_full):
    expression_statements = []
    for key in fixations:
        for f in fixations[key]:
            aoi_type = f.get('element-id').split('-')[1]
        
            if aoi_type == 'expression_statement':
                expression_statements.append(f)

    aoiList = []  
    for f in expression_statements:
        file_name = f.get('element-id').split(':')[-1]

        found = False
        for key in parser_response_full:
            if found:
                break

            for response in parser_response_full[key]:
                file_name_parser = response.get('FileName')

                if file_name == file_name_parser:
                    ast = response.get('Response')
                    aoiList.extend(get_aoi_children_recursive(f, ast))
                    found = True
                    break

    keys, counts = np.unique(aoiList, return_counts=True)

    sorted_indices = np.argsort(counts)[::-1]
    sorted_keys = keys[sorted_indices]
    sorted_counts = counts[sorted_indices]

    print(sorted_keys)
    print(sorted_counts)

    plot_keys = []
    plot_counts = []
    for i in range(len(sorted_keys)):
        if sorted_counts[i] > 5000:
            plot_keys.append(sorted_keys[i])
            plot_counts.append(sorted_counts[i])
            print(sorted_keys[i])
            print(sorted_counts[i])

    plt.bar(plot_keys, plot_counts)
    plt.show()

### bar plot, contents of expression_statements (limited by a depth) that are fixated on
def contents_of_expression_statements_at_depth(fixations, parser_response_full, depth):
    expression_statements = []
    for key in fixations:
        for f in fixations[key]:
            aoi_type = f.get('element-id').split('-')[1]
        
            if aoi_type == 'expression_statement':
                expression_statements.append(f)

    aoiList = []  
    for f in expression_statements:
        file_name = f.get('element-id').split(':')[-1]

        found = False
        for key in parser_response_full:
            if found:
                break

            for response in parser_response_full[key]:
                file_name_parser = response.get('FileName')

                if file_name == file_name_parser:
                    ast = response.get('Response')
                    aoiList.extend(get_aoi_children_recursive(f, ast, depth))
                    found = True
                    break

    keys, counts = np.unique(aoiList, return_counts=True)

    sorted_indices = np.argsort(counts)[::-1]
    sorted_keys = keys[sorted_indices]
    sorted_counts = counts[sorted_indices]

    print(sorted_keys)
    print(sorted_counts)

    plot_keys = []
    plot_counts = []
    for i in range(len(sorted_keys)):
        if sorted_counts[i] >= 1:
            plot_keys.append(sorted_keys[i])
            plot_counts.append(sorted_counts[i])
            print(sorted_keys[i])
            print(sorted_counts[i])

    plt.bar(plot_keys, plot_counts)
    plt.show()

### bar plot, contents of expression_statements (limited by a depth)
def children_of_all_expression_statements(parser_response_full):
    aoiList = []
    for key in parser_response_full:
        for response in parser_response_full[key]:
            ast = response.get('Response')
            aoiList.extend(get_children_of_aoi_type_recursive(ast, 'expression_statement'))

    keys, counts = np.unique(aoiList, return_counts=True)

    sorted_indices = np.argsort(counts)[::-1]
    sorted_keys = keys[sorted_indices]
    sorted_counts = counts[sorted_indices]

    print(sorted_keys)
    print(sorted_counts)

    plot_keys = []
    plot_counts = []
    for i in range(len(sorted_keys)):
        if sorted_counts[i] >= 0:
            plot_keys.append(sorted_keys[i])
            plot_counts.append(sorted_counts[i])
            print(sorted_keys[i])
            print(sorted_counts[i])

    plt.bar(plot_keys, plot_counts)
    plt.show()