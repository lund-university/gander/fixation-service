import seaborn as sns

### returns a dictionary with aoi types and colors, made with Java grammar in mind
def get_aoi_colors():
    aoi_names = [
        "expression_statement", "string_literal", "local_variable_declaration", "condition", "identifier",
        "import_declaration", "return_statement", "line_comment", "argument_list", "field_declaration",
        "formal_parameters", "method_invocation", "cast_expression", "type_identifier", "modifiers",
        "generic_type", "throw_statement", "enhanced_for_statement", "formal_parameter", "lambda_expression"
    ]

    color_palette = [
        "red", "green", "blue", "orange", "purple", "cyan", "magenta", "yellow", "pink", "lime", 
        "brown", "dodgerblue", "olive", "gold", "teal", "salmon", "navy", "chocolate", "orchid", 
        "turquoise", "crimson", "darkgreen", "darkorange", "deepskyblue", "slateblue", "firebrick"
    ]
    
    assert len(aoi_names) <= len(color_palette), "Not enough colors for the number of AOIs"

    aoi_colors = {aoi: color_palette[i] for i, aoi in enumerate(aoi_names)}

    return aoi_colors

### returns a list of colors of specified size using seaborn
def get_aoi_colors(n, palette="deep"):
    return sns.color_palette(palette, n).as_hex()

# Plots coverage with different colors depending on wether coverage increased or decreased (show-more event)
def plot_steps_with_color(ax, times, coverage):
    inc_times = []
    inc_coverage = []
    dec_vertical_lines = []

    for i in range(1, len(times)):
        if coverage[i] >= coverage[i-1]:
            inc_times.append([times[i-1], times[i], times[i]])
            inc_coverage.append([coverage[i-1], coverage[i-1], coverage[i]])
        else:
            inc_times.append([times[i-1], times[i], times[i]])
            inc_coverage.append([coverage[i-1], coverage[i-1], coverage[i]])
            dec_vertical_lines.append(((times[i], times[i]), (coverage[i-1], coverage[i])))

    for t, c in zip(inc_times, inc_coverage):
        ax.step(t, c, where='post', color='blue')
    
    for v_line in dec_vertical_lines:
        ax.plot(v_line[0], v_line[1], color='red')