# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 19:25:01 2021

@author: Marcus
Extract trial data and save it in a format that I2MC accepts to detect fixations.

Adapted to handle data recorded from the read_my.py demo. Each .pkl-file
contains data from one participant

"""
import h5py
import numpy as np
import pandas as pd
import os
from pathlib import Path
import errno
import json

import sys
import App.service.import_funcs as imp
try:
    import I2MC
    import I2MC.plot
except ImportError:
    raise ImportError('I2MC not found. pip install I2MC')
import matplotlib.pyplot as plt
import time

# %%
def extract_trial_data(df_et_data, df_msg, msg_onset, msg_offset, onset_count):
    ''' Extracts data from one trial associated with the
    stimulus name.

    Args:
        df_et_data - Pandas dataframe with sample et-data (output from Titta)
        df_msg - Pandas dataframe containing messages (assume stimname is 'my_im.png')
        msg_onset (str) - message sent at stimulus onset
        msg_offset (str) - message sent at stimulus onset


    Returns:
        df - dataframe with data from one trial

    '''

    # Find timestamps for data belonging to this stimulus
    start_idx = np.where(df_msg.msg == msg_onset)[0][onset_count]
    stop_idx = np.where(df_msg.msg == msg_offset)[0][onset_count]

    start_time_stamp = df_msg.system_time_stamp[start_idx]
    stop_time_stamp = df_msg.system_time_stamp[stop_idx]

    # print(start_idx, stop_idx, start_time_stamp, stop_time_stamp)
    #
    # Cut out samples belonging to this stimulus
    fix_idx_start = np.searchsorted(df_et_data.system_time_stamp,
                                    start_time_stamp)
    fix_idx_stop = np.searchsorted(df_et_data.system_time_stamp,
                                   stop_time_stamp)
    df_stim = df_et_data.iloc[fix_idx_start:fix_idx_stop].copy()

    return df_stim

# %%

# Read messages and et data all participants (one .pkl-file per participant)
cwd = os.path.abspath(os.getcwd())
cwd = os.path.split(cwd)[0]
cwd = os.path.join(cwd, 'App')
cwd = os.path.join(cwd, 'sessionData')

#files=os.listdir(cwd)
files=['2024120202_54_34.h5']

for f in files:

    pid = str(f).split(os.sep)[-1][:-3]

    # Convert to pandas dataframes
    print(f)
    df_gaze = pd.read_hdf(os.path.join(cwd, f), 'gaze')
    df_msg= pd.read_hdf(os.path.join(cwd, f), 'msg')

    # Read message for onset and offset
    # Assumption is that messages are on the form (must be unique)
    # 'onset_stimulusname' for the onset of a stimulus and
    # 'offset_stimulusname'for the offset of a stimulus
    onset = []
    offset = []
    for i, row in df_msg.iterrows():
        if 'onset' in row.msg:
            onset.append(row.msg)
        if 'offset' in row.msg:
            offset.append(row.msg)
    trial_msg = zip(onset, offset)

    # Create a folder to put the trials
    path = Path.cwd() / 'trials' / pid
    try:
        path.mkdir(parents=True, exist_ok=False)
    except FileExistsError:
        print("Folder is already there")
    else:
        print("Folder was created")

    onset_counter = {}

    # Extract relevant trial data and save in format required by I2MC
    for t in trial_msg:
        onset_msg, offset_msg = t

        if onset_msg not in onset_counter:
            onset_counter[onset_msg] = 0

        df_trial = extract_trial_data(df_gaze, df_msg, onset_msg, offset_msg, onset_counter[onset_msg])

        onset_counter[onset_msg] += 1

        df_trial.reset_index(inplace=True)

        filename = t[0].split('_', 1)[1] + '.tsv'
        file_path = str(path) + os.sep + filename

        file_exists = os.path.isfile(file_path)
        
        df_trial.to_csv(file_path, sep='\t', mode='a', header=not file_exists, index=False)

        if file_exists:
            print('Trial ' + filename + " appended to folder ", path)
        else:
            print('Trial ' + filename + " written to folder ", path)


# Folder where the trails should be

trial_folder = Path.cwd() / 'trials'

#%% Compute data quality from pickle

#files = (Path.cwd() / 'data').glob('*.h5')

data_quality = []
info = []
for f in files:

    print(f)

    # Load info about tracker
    fh = open('sessionData\\'+str(f)[:-3] + '_info.json')
    info.append(json.load(fh))

    pid = str(f).split(os.sep)[-1][:-3]

    # Convert to pandas dataframes
    calibration_history = pd.read_hdf('sessionData\\'+f, 'calibration_history')

    # Go through all calibrations and select 'used' ones.
    # 'Used' means that it was the selected calibration
    # (several calibration can be made and the best one can
    # be selected)
    # All data quality values here are recorded during the
    # validation procedure following directly after the calibration.
    for i, row in calibration_history.iterrows():
        # if this calibration was 'used'?
        if row['Calibration used'] == 'used':
            data_quality.append([pid] + list(row.to_numpy()[:-1]))

    # Also compute precision and data loss from individual trials


df = pd.DataFrame(data_quality, columns=['pid',
                                         'accuracy_left_eye (deg)',
                                         'accuracy_right_eye (deg)',
                                         'RMS_S2S_left_eye (deg)',
                                         'RMS_S2S_right_eye (deg)',
                                         'SD_left_eye (deg)',
                                         'SD_right_eye (deg)',
                                         'Prop_data_loss_left_eye',
                                         'Prop_data_loss_right_eye'])


# Create a new folder (if it does not exist)
path = Path.cwd() / 'data_quality'
Path(path).mkdir(parents=True, exist_ok=True)

df.to_csv(path / 'data_quality_validation.csv')
print("Data quality values written to {path / 'data_quality_validation'}")


# %% Compute data loss per participant and trial.

# A requirement to run this analysis is that data already have been divided
# into trials (by running extract_trial_data.py). Check if that have been done
if  not trial_folder.exists():
    raise FileNotFoundError(
        errno.ENOENT, os.strerror(errno.ENOENT) + '. ***OBS! You must first run extract_trial_data.py***', trial_folder)

# Go through all trials and compute data loss for each trial
data_loss_trials = []
trials = trial_folder.rglob('*.tsv')
for trial in trials:
    pid = str(trial).split(os.sep)[-2]

    df_trial = pd.read_csv(trial, sep='\t')

    if len(df_trial) == 0:
        print('Warning: trial with no data')
        continue

    trial_name = '.'.join(str(trial).split(os.sep)[-1].split('.')[:2])
    print(df_trial.system_time_stamp.iloc)
    print(info)
    # Check that no samples are missing (based on the expected number of samples)
    expected_number_of_samples = (df_trial.system_time_stamp.iloc[-1] - df_trial.system_time_stamp.iloc[0]) / 1000 / 1000 * info[0]['sampling_frequency']
    recorded_number_of_samples = len(df_trial.system_time_stamp)
    percent_valid_samples = recorded_number_of_samples/expected_number_of_samples*100
    if percent_valid_samples < 99:
        print(f'WARNING: Trial is missing {100 - percent_valid_samples}% of the samples.')
    prop_missing_data = 1 - recorded_number_of_samples/expected_number_of_samples
    if np.abs(prop_missing_data) < 0.001:
        prop_missing_data = 0

    # Compute precision
    for eye in  ['left', 'right']:
        n_samples = len(df_trial)
        n_valid_samples = np.nansum(df_trial[eye + '_gaze_point_valid'])
        prop_invalid_samples = 1 - n_valid_samples / n_samples
        data_loss_trials.append([pid, trial_name, eye, n_samples,
                                 n_valid_samples, prop_invalid_samples, prop_missing_data])

df_trial = pd.DataFrame(data_loss_trials, columns=['pid', 'trial',
                                                      'eye',
                                                      'n_trial_samples',
                                                      'n_valid_trial_samples',
                                                      'prop_invalid_samples',
                                                      'prop_missing_data'])


df_trial.to_csv(path / 'data_loss_per_trial.csv')
print(f"Data loss values written to {path / 'data_loss_per_trial.csv'}")



start = time.time()

# =============================================================================
# NECESSARY VARIABLES
# =============================================================================
opt = {}
# General variables for eye-tracking data
opt['xres']         = 1920.0                # maximum value of horizontal resolution in pixels
opt['yres']         = 1080.0                # maximum value of vertical resolution in pixels
opt['missingx']     = -opt['xres']          # missing value for horizontal position in eye-tracking data (example data uses -xres). used throughout the algorithm as signal for data loss
opt['missingy']     = -opt['yres']          # missing value for vertical position in eye-tracking data (example data uses -yres). used throughout algorithm as signal for data loss
opt['freq']         = 90.0                 # sampling frequency of data (check that this value matches with values actually obtained from measurement!)

# Variables for the calculation of visual angle
# These values are used to calculate noise measures (RMS and BCEA) of
# fixations. The may be left as is, but don't use the noise measures then.
# If either or both are empty, the noise measures are provided in pixels
# instead of degrees.
opt['scrSz']        = [52.7, 30]    # screen size in cm
opt['disttoscreen'] = 63.0                  # distance to screen in cm.

# Folders
# Data folder should be structured by one folder for each participant with
# the eye-tracking data in textfiles in each folder.
dir_path = os.path.dirname(os.path.realpath(__file__))
folders  = {}
folders['data']   = os.path.join(dir_path,'trials')   # folder in which data is stored (each folder in folders.data is considered 1 subject)
folders['output'] = os.path.join(dir_path,'output')         # folder for output (will use structure in folders.data for saving output)

# Options of example script
log_level    = 1    # 0: no output, 1: output from this script only, 2: provide some output on command line regarding I2MC internal progress
do_plot_data = True # if set to True, plot of fixation detection for each trial will be saved as png-file in output folder.
# the figures works best for short trials (up to around 20 seconds)

# =============================================================================
# OPTIONAL VARIABLES
# =============================================================================
# The settings below may be used to adopt the default settings of the
# algorithm. Do this only if you know what you're doing.

# # STEFFEN INTERPOLATION
opt['windowtimeInterp']     = 0.1                           # max duration (s) of missing values for interpolation to occur
opt['edgeSampInterp']       = 2                             # amount of data (number of samples) at edges needed for interpolation
opt['maxdisp']              = opt['xres']*0.2*np.sqrt(2)    # maximum displacement during missing for interpolation to be possible

# # K-MEANS CLUSTERING
opt['windowtime']           = 0.2                           # time window (s) over which to calculate 2-means clustering (choose value so that max. 1 saccade can occur)
opt['steptime']             = 0.02                          # time window shift (s) for each iteration. Use zero for sample by sample processing
opt['maxerrors']            = 100                           # maximum number of errors allowed in k-means clustering procedure before proceeding to next file
opt['downsamples']          = [2, 5, 10]
opt['downsampFilter']       = True                         # use chebychev filter when downsampling? Its what matlab's downsampling functions do, but could cause trouble (ringing) with the hard edges in eye-movement data

# # FIXATION DETERMINATION
opt['cutoffstd']            = 2.0                           # number of standard deviations above mean k-means weights will be used as fixation cutoff
opt['onoffsetThresh']       = 3.0                           # number of MAD away from median fixation duration. Will be used to walk forward at fixation starts and backward at fixation ends to refine their placement and stop algorithm from eating into saccades
opt['maxMergeDist']         = 30.0                          # maximum Euclidean distance in pixels between fixations for merging
opt['maxMergeTime']         = 30.0                          # maximum time in ms between fixations for merging
opt['minFixDur']            = 40.0                          # minimum fixation duration after merging, fixations with shorter duration are removed from output
opt['chebyOrder']           = 8

# Change parameters according to the recommendations on Github
if opt['freq'] == 120:
    opt['downsamples']          = [2, 3, 5]
    opt['chebyOrder']           = 7

if opt['freq'] < 120:
    opt['downsamples']          = [2, 3]
    opt['downsampFilter']       = False


# =============================================================================
# SETUP directory handling
# =============================================================================
# Check if output directory exists, if not create it
if not os.path.isdir(folders['output']):
   os.mkdir(folders['output'])

# Get all participant folders
fold = list(os.walk(folders['data']))
all_folders = [f[0] for f in fold[1:]]
number_of_folders = len(all_folders)

# Get all files
all_files = [f[2] for f in fold[1:]]
number_of_files = [len(f) for f in all_files]

# Write the final fixation output file
fix_file = os.path.join(folders['output'], 'allfixations.txt')
for it in range(1,101):
    if os.path.isfile(fix_file) and it < 100:
        fix_file = os.path.join(folders['output'], 'allfixations_{}.txt'.format(it))
    else:
        if log_level>0:
            print('Fixations will be stored to: "{}"'.format(fix_file))
        break

# =============================================================================
# START ALGORITHM
# =============================================================================
for folder_idx, folder in enumerate(all_folders):
    if log_level>0:
        print('Processing folder {} of {}'.format(folder_idx + 1, number_of_folders))

    # make output folder
    if do_plot_data:
        outFold = os.path.join(folders['output'], (folder.split(os.sep)[-1]))
        if not os.path.isdir(outFold):
           os.mkdir(outFold)

    if number_of_files[folder_idx] == 0:
        if log_level>0:
            print('  folder is empty, continuing to next folder')
        continue

    for file_idx, file in enumerate(all_files[folder_idx]):
        if log_level>0:
            print('  Processing file {} of {}'.format(file_idx + 1, number_of_files[folder_idx]))

        # Get current file name
        file_name = os.path.join(folder, file)
        ## IMPORT DATA
        if log_level>0:
            print('    Loading data from: {}'.format(file_name))
        data = imp.Titta(file_name, [opt['xres'], opt['yres']])

        # check whether we have data, if not, continue to next file
        if len(data['time']) == 0:
            if log_level>0:
                print('    No data found in file')
            continue

        # RUN FIXATION DETECTION
        if log_level>0:
            print('    Running fixation classification...')
        try:
            fix,_,_ = I2MC.I2MC(data,opt,log_level==2,logging_offset="      ")
        except Exception as e:
            print('    Error in file {}: {}'.format(file_name, e))
            continue

        if not fix:
            if log_level>0:
                print('    Fixation classification did not succeed with file {}'.format(file_name))
            continue

        if fix != False:
            ## PLOT RESULTS
            if do_plot_data:
                # pre-allocate name for saving file
                save_file = os.path.join(outFold, os.path.splitext(file)[0]+'.png')
                f = I2MC.plot.data_and_fixations(data, fix, fix_as_line=True, res=[opt['xres'], opt['yres']])
                # save figure and close
                if log_level>0:
                    print('    Saving image to: ' + save_file)
                f.savefig(save_file)
                plt.close(f)

            # Write data to file
            fix['participant'] = folder.split(os.sep)[-1]
            fix['trial'] = os.path.splitext(file)[0]
            fix_df = pd.DataFrame(fix)
            fix_df.to_csv(fix_file, mode='a', header=not os.path.exists(fix_file),
                          na_rep='nan', sep='\t', index=False, float_format='%.3f')

print('\n\nI2MC took {}s to finish!'.format(time.time()-start))
