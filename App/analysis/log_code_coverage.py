from log_utility_functions import *
import numpy as np
import matplotlib.pyplot as plt

# Plots code coverage over time accounted for visibility
def code_coverage_over_time(fixations, parser_response, diff_lines, file_patches, show_more_events):
    data = {}

    visibility_periods = get_visibility_periods(file_patches, show_more_events)
    file_lengths = get_file_lengths(parser_response)
    adjusted_lines = get_diff_adjusted_lines(diff_lines, file_lengths)

    aois = calculate_aoi_set_per_visibility_period(parser_response, visibility_periods, diff_lines)

    files = {}
    startTimes = {}
    endTimes = {}
    unique_fixations = {}
    for key in fixations:
        files[key] = {}
        startTimes[key] = {}
        endTimes[key] = 0
        unique_fixations[key] = {}
        startTime = 0
        for f in fixations[key]:
            aoi_type = f.get('element-id')
            file_name = f.get('element-id').split(':')[-1]
            cleaned_file_name = file_name.replace('-src', '').replace('-target', '')
            line_number = extract_line_number(aoi_type)
            time = int(f.get('msSinceEpoch'))

            if startTime == 0:
                startTime = time

            startTimes[key][cleaned_file_name] = startTime

            if cleaned_file_name not in unique_fixations[key]:
                unique_fixations[key][cleaned_file_name] = set()

            if line_number in diff_lines[file_name]:
                unique_fixations[key][cleaned_file_name].add(aoi_type)
            else:
                adjusted_line_number = adjusted_lines[file_name][line_number]
                aoi_replaced_number = replace_line_number(aoi_type.replace('-src', '').replace('-target', ''), adjusted_line_number)

                unique_fixations[key][cleaned_file_name].add(aoi_replaced_number)

            count = get_aoi_count_for_time(aois[key][cleaned_file_name], time)
            ratio = len(unique_fixations[key][cleaned_file_name]) / count

            time = time - startTime
            if cleaned_file_name in files[key]:
                files[key][cleaned_file_name].append((time, ratio))
                data[f'{key}/code_coverage_over_time/{cleaned_file_name}'].append((time, ratio))
            else:
                files[key][cleaned_file_name] = [(0, 0)]
                files[key][cleaned_file_name].append((time, ratio))
                data[f'{key}/code_coverage_over_time/{cleaned_file_name}'] = [(0, 0)]
                data[f'{key}/code_coverage_over_time/{cleaned_file_name}'].append((time, ratio))

            endTimes[key] = max(endTimes[key], time)
        
        data[f'{key}/code_coverage_over_time/end_times'] = endTimes[key]

    return data

# Calculates the code coverage for each session, i.e. across all files in the PR, accounted for visibility
def code_coverage_visibility(fixations, parser_response, diff_lines, file_patches, show_more_events):
    data = {}

    visibility_periods = get_visibility_periods(file_patches, show_more_events)
    file_lengths = get_file_lengths(parser_response)
    adjusted_lines = get_diff_adjusted_lines(diff_lines, file_lengths)

    aois = calculate_aoi_set_per_visibility_period(parser_response, visibility_periods, diff_lines)
 
    fixations_per_period = {}

    for key in fixations:
        fixations_per_period[key] = {}

        for f in fixations[key]:
            aoi_type = f.get('element-id')
            file_name = f.get('element-id').split(':')[-1]
            cleaned_file_name = file_name.replace('-src', '').replace('-target', '')
            line_number = extract_line_number(aoi_type)
            time = f.get('msSinceEpoch')

            if cleaned_file_name not in fixations_per_period[key]:
                fixations_per_period[key][cleaned_file_name] = {}

            for period in visibility_periods[key][file_name]:
                from_time = period['from']
                to_time = period['to']

                if from_time not in fixations_per_period[key][cleaned_file_name]:
                    fixations_per_period[key][cleaned_file_name][from_time] = set()

                if from_time <= int(time) and int(time) <= to_time:
                    if line_number in diff_lines[file_name]:
                        fixations_per_period[key][cleaned_file_name][from_time].add(aoi_type)
                    else:
                        adjusted_line_number = adjusted_lines[file_name][line_number]
                        aoi_replaced_number = replace_line_number(aoi_type.replace('-src', '').replace('-target', ''), adjusted_line_number)

                        fixations_per_period[key][cleaned_file_name][from_time].add(aoi_replaced_number)
           
    for key in aois:
        #print(f'for log file: {key}')

        total_fixations = 0
        total_aois = 0

        for file_name in fixations_per_period[key]:
            total_aoi_file = set()
            for period in fixations_per_period[key][file_name]:
                total_aoi_file.update(fixations_per_period[key][file_name][period])
                
            total_fixations += len(total_aoi_file)

        for file_name in aois[key]:
            total_aois += len(list(aois[key][file_name].items())[-1][1])

        data[f'{key}/code_coverage/visible_lines'] = total_fixations/total_aois

        #print(f'Code Coverage accounted for code visibility: {total_fixations/total_aois*100}%')

    return data

def code_coverage_changes(fixations, parser_response, diff_lines):
    data = {}
    aois={}
    for key in parser_response:
        aois[key]=[{},0]
        for f in parser_response[key]:
            ast = f.get('Response')
            aois_in_file = 0

            if '-target' in f.get('FileName') or '-src' in f.get('FileName'):
                aois_in_file = recursive_aoi_count_in_lines(ast, diff_lines[f.get('FileName')])

            file_name = f.get('FileName')
            aois[key][0][file_name] = aois_in_file

    for key in fixations:
        count = set()
        for f in fixations[key]:
            id = f.get('element-id')
            file_name = f.get('element-id').split(':')[-1]
            line_number = extract_line_number(id)

            if line_number in diff_lines[file_name]:
                count.add(id)
           
        aois[key][1] = len(count)
    
    for key in aois:
        #print(f'for log file: {key}')

        sum = 0
        for file in aois[key][0]:
            sum += aois[key][0][file]

        #print(f'Code Coverage: {(aois[key][1]/sum)*100}%')
        data[f'{key}/code_coverage/changes_lines'] = aois[key][1]/sum

    return data

## Calculates the code coverage for each session, i.e. across all files in the PR
def code_coverage(fixations, parser_response, diff_lines):
    data = {}

    file_lengths = get_file_lengths(parser_response)
    adjusted_lines = get_diff_adjusted_lines(diff_lines, file_lengths)

    aois={}
    for key in parser_response:
        aois[key]=[{},0]
        for f in parser_response[key]:
            ast = f.get('Response')
            aois_in_file = set()

            if '-target' in f.get('FileName'):
                aois_in_file = recursive_aoi_set(ast)
            elif '-src' in f.get('FileName'):
                aois_in_file = recursive_aoi_set_in_lines(ast, diff_lines[f.get('FileName')])

            file_name = f.get('FileName').replace('-src', '').replace('-target', '').replace('-fileview', '')

            if file_name in aois[key][0]:
                aois[key][0][file_name].update(aois_in_file)
            else:
                aois[key][0][file_name] = aois_in_file

    for key in fixations:
        count = set()
        for f in fixations[key]:
            aoi_type = f.get('element-id')
            file_name = f.get('element-id').split(':')[-1]
            line_number = extract_line_number(aoi_type)

            if line_number in diff_lines[file_name]:
                count.add(aoi_type)
            else:
                adjusted_line_number = adjusted_lines[file_name][line_number]
                count.add(replace_line_number(aoi_type.replace('-src', '').replace('-target', ''), adjusted_line_number))
           
        aois[key][1] = len(count)
    
    for key in aois:
        sum = 0
        for file in aois[key][0]:
            sum += len(aois[key][0][file])

        data[f'{key}/code_coverage/all_lines'] = aois[key][1]/sum

    return data

## Calculates line coverage 
def line_coverage(fixations, parser_response):
    data = {}

    file_lenghts = get_file_lengths_longest(parser_response)

    lines = {}
    for key in fixations:
        lines[key]={}
        for f in fixations[key]:
            file_name = f.get('element-id').split('-')[-2].split(':')[-1]
            file_line = f.get('element-id').split('-')[3].split(':')[0]

            if file_name in lines[key]:
                lines[key][file_name].add(file_line)
            else:
                lines[key][file_name] = set([file_line])

    for key in lines:
        covered_lines = 0
        for file in lines[key]:
            covered_lines += len(lines[key][file])

        total_lines = 0
        for f in parser_response[key]:
            if '-fileview' not in f.get('FileName') and '-src' not in f.get('FileName'):
                file_name = f.get('FileName').replace('-target', '')
                total_lines += file_lenghts[file_name]

        data[f'{key}/line_coverage/all_lines'] = covered_lines/total_lines

    return data

## Calculates line coverage 
def changed_line_coverage(fixations, parser_response, diff_lines):
    data = {}

    lines = {}
    for key in fixations:
        lines[key]={}
        for f in fixations[key]:
            file_name = f.get('element-id').split(':')[-1]
            file_line = f.get('element-id').split('-')[3].split(':')[0]

            if file_name in lines[key]:
                lines[key][file_name].add(file_line)
            else:
                lines[key][file_name] = set([file_line])

    all_lines = {}
    for key in parser_response:
        all_lines[key] = {}
        for f in parser_response[key]:
            if '-fileview' not in f.get('FileName'):
                all_lines[key][f.get('FileName')] = len(diff_lines[f.get('FileName')])

    for key in lines:
        #print(f'for log file: {key}')

        covered_lines = 0
        total_lines = 0
        for file in lines[key]:
            covered_lines += len(diff_lines[file].intersection(lines[key][file]))
        
        for file in all_lines[key]:
            total_lines += all_lines[key][file]

        #print(f'Changed line Coverage: {(covered_lines/total_lines)*100}%')
        data[f'{key}/line_coverage/changed_lines'] = covered_lines/total_lines

    return data

