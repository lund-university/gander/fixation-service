from log_code_coverage import *
from log_utility_functions import *
import numpy as np
import os
from pathlib import Path
import json
import re
import matplotlib.pyplot as plt
import pandas as pd
import h5py

#calculates some metrics used to characterize the code base,
# nbr files, distribution of AOIs
def code_base_metrics(parser_response): 
    data={}
    files=[]
    for key in parser_response:
        pr=key[-1:]
        num_files=0

        for f in parser_response[key]:
            if(pr+f.get('FileName') not in files):
                files.append(pr+f.get('FileName'))
            else:
                continue
            ast = f.get('Response')
            num_files+=1
            start=0
            end=ast.get('end').split(':')[0]
            
            lines=range(int(start),int(end))
            aois=recursive_aoi_list_in_lines(ast, lines)
            keys, counts = np.unique(aois, return_counts=True)
            l=5 if len(keys)>5 else len(keys)
            data[f'code_base_metrics/change{pr}/{f.get("FileName")}/LOC']=len(lines)
            data[f'/code_base_metrics/change{pr}/{f.get("FileName")}/NUMUNIQUEAOI']=len(keys)   

            aoi_types=[]
            aoi_counts=[]
            for i in range(l):
                idx= np.argmax(counts)
                aoi_types.append(str(keys[idx]))
                aoi_counts.append(int(counts[idx]))
                counts[idx]=0
            data[f'code_base_metrics/change{pr}/{f.get("FileName")}/dist_AOI/types']=aoi_types
            data[f'code_base_metrics/change{pr}/{f.get("FileName")}/dist_AOI/counts']=aoi_counts
            data[f'code_base_metrics/change{pr}/{f.get("FileName")}/dist_AOI/totalAOI']=len(aois)
        if(not f'code_base_metrics/change{pr}/num_files' in data):
            data[f'code_base_metrics/change{pr}/num_files']=num_files

    return data

#finds out the percentage of src and target fixations for each participant
def src_vs_target(fixations):
    data = {}
    for key in fixations:
        #print(f'for logfile: {key}')
        length = len(fixations[key])
        src =0
        target=0
        for f in fixations[key]:
            if '-target' in f.get('element-id'):
                target+=1
            else:
                src+=1
        data[f'{key}/src_vs_target/src']=src
        data[f'{key}/src_vs_target/target']=target
        data[f'{key}/src_vs_target/total']=length
    return data
        #print(f'Number of fixations on source: {src} \n percentage: {src/length*100}%')
        #print(f'Number of fixations on target: {target} \n percentage: {target/length*100}%')

def save_visibility_periods(file_patches, show_more_events):
    data = {}
    visibility_periods = get_visibility_periods(file_patches, show_more_events)

    for keys in visibility_periods:
        for file in visibility_periods[keys]:
            structured_visibility_period = np.empty(len(visibility_periods[keys][file]), dtype=visibility_period_type)
            for i, period in enumerate(visibility_periods[keys][file]):
                structured_visibility_period[i] = (
                    period['start'],
                    period['end'],
                    period['from'],
                    period['to']
                )
            
            data[f'{keys}/visibility_periods/{file}']=structured_visibility_period
                    
    return data

# LOC over time for each file and participant
def loc_over_time(fixations, show_more_events, file_patches, file_lengths):
    data = {}

    files = {}
    start_times = {}
    for key in fixations:
        files[key] = {}
        start_times[key] = {}
        startTime = 0
        prev_file = None
        for f in fixations[key]:
            file_name = f.get('element-id').split('-')[-2].split(':')[-1]
            file_line = int(f.get('element-id').split('-')[3].split(':')[0])

            if prev_file != None and prev_file!=file_name:
                data[f'{key}/loc_over_time/{prev_file}'].append((time + 1000, float("NaN")))
                files[key][prev_file].append((time+1000, float("NaN")))

            time = int(f.get('msSinceEpoch'))
            if startTime == 0:
                startTime = time

            start_times[key][file_name] = startTime
            data[f'{key}/loc_over_time/start_times/{file_name}'] = startTime

            time = time-startTime
            if file_name in files[key]:
                data[f'{key}/loc_over_time/{file_name}'].append((time,file_line))
                files[key][file_name].append((time,file_line))
            else:
                data[f'{key}/loc_over_time/{file_name}']=[(time,file_line)]
                files[key][file_name]=[(time,file_line)]

            data[f'{key}/loc_over_time/file_lengths/{file_name}'] = max(file_lengths[file_name + '-src'], file_lengths[file_name + '-target'])

            prev_file=file_name

    return data

### data for aoi type over time, can be used to make a scarf plot
def aoi_over_time(fixations):
    data = {}
    files = {}
    for key in fixations:
        files[key] = []
        data[f'{key}/aoi_over_time'] = []
        startTime = 0

        for f in fixations[key]:
            aoi_type = f.get('element-id').split('-')[1]

            time = int(f.get('msSinceEpoch'))
            if startTime == 0:
                startTime = time
            time = time - startTime

            files[key].append((time, aoi_type))
            data[f'{key}/aoi_over_time'].append((time, aoi_type))

        # We convert to a structured NumPy array for compatibility with HDF5 storage
        data[f'{key}/aoi_over_time'] = np.array(
            data[f'{key}/aoi_over_time'],
            dtype=[('time', 'i4'), ('aoi_type', 'S50')]
        )

    return data

# Bar plot of fixations on each AOI for all participants. Divided into visible, changed, and unchanged.
def cumulative_fixations_per_aoi(
        fixations, 
        parser_response, 
        visibility_periods, 
        ):
    
    visible_lines = get_visible_lines(visibility_periods,  get_file_lengths(parser_response))
    changed_lines = {}
    unchanged_lines = {}
    for key in visible_lines:
        changed_lines[key] = diff_lines

        unchanged_lines[key] = {}
        for file in visible_lines[key]:
            unchanged_lines[key][file] = visible_lines[key][file] - changed_lines[key][file]    

    number_of_bars = 9
    fixation_treshold = 1000
    normalize_by_aoi_length = True
    normalize_by_aoi_occurance = True  
    sort_first = False
    mark_multiline = True
    top = True

    cumulative_fixations_per_aoi_barplot(
        fixations, 
        parser_response, 
        changed_lines,
        title = 'Changed lines',
        n = number_of_bars, 
        fixation_treshold = fixation_treshold,
        normalize_by_aoi_length = normalize_by_aoi_length, 
        normalize_by_aoi_occurance = normalize_by_aoi_occurance, 
        sort_first = sort_first, 
        mark_multiline = mark_multiline,
        top = top
        )
    
    cumulative_fixations_per_aoi_barplot(
        fixations, 
        parser_response, 
        unchanged_lines,
        title = 'Unchanged lines',
        n = number_of_bars,
        fixation_treshold = fixation_treshold, 
        normalize_by_aoi_length = normalize_by_aoi_length, 
        normalize_by_aoi_occurance = normalize_by_aoi_occurance, 
        sort_first = sort_first, 
        mark_multiline = mark_multiline,
        top = top
        )
    
    cumulative_fixations_per_aoi_barplot(
        fixations, 
        parser_response, 
        visible_lines,
        title = 'Visible lines',
        n = number_of_bars, 
        fixation_treshold = fixation_treshold,
        normalize_by_aoi_length = normalize_by_aoi_length, 
        normalize_by_aoi_occurance = normalize_by_aoi_occurance, 
        sort_first = sort_first, 
        mark_multiline = mark_multiline,
        top = top
        )
    
    plt.show()

# Bar plot of fixations on each AOI for all participants.
def cumulative_fixations_per_aoi_barplot(
        fixations, 
        parser_response, 
        target_lines, 
        title,
        n, 
        fixation_treshold,
        normalize_by_aoi_length, 
        normalize_by_aoi_occurance, 
        sort_first, 
        mark_multiline,
        top
        ):
    
    aois_in_files = []
    if normalize_by_aoi_occurance:
        visited_files = set()

        for key in parser_response:
            for f in parser_response[key]:
                if f.get('FileName') not in visited_files and '-fileview' not in f.get('FileName'):
                    visited_files.add(f.get('FileName'))
                    aois_in_files.extend(recursive_aoi_list_in_lines(f.get('Response'), target_lines[key][f.get('FileName')]))

    aoiLists = []
    aoi_lengths = {}
    aoi_multiline = {}
    size_map = map_aoi_to_size(parser_response)

    for key in fixations:
        for f in fixations[key]:
            identifier = re.sub(r'-parent:[^-]*', '', f.get('element-id'))
            match = re.search(r'-file:(.*?-(target|src))', identifier)
            file_name = match.group(1)
            aoi_type = identifier.split('-')[1]
            size_details = size_map[identifier]
            size = size_details[4]

            if size_details[0] in target_lines[key][file_name]:
                if aoi_type in aoi_lengths:
                    aoi_lengths[aoi_type].append(size)
                    aoi_multiline[aoi_type].append(size_details[2] - size_details[0] > 0)
                else:
                    aoi_lengths[aoi_type] = [size]
                    aoi_multiline[aoi_type] = [size_details[2] - size_details[0] > 0]

                aoiLists.append(aoi_type)

    aoi_avg_lengths = {aoi: np.mean(sizes) for aoi, sizes in aoi_lengths.items()}

    for aoi_type in aoi_multiline:
        bool_list = aoi_multiline[aoi_type]
        aoi_multiline[aoi_type] = sum(bool_list) / len(bool_list) >= 0.05

    keys, counts = np.unique(aoiLists, return_counts=True)
    
    if sort_first:
        sorted_indices = np.argsort(counts)[::-1]
        keys = keys[sorted_indices]
        counts = counts[sorted_indices]

    if normalize_by_aoi_occurance:
        keys_total, counts_total = np.unique(aois_in_files, return_counts=True)
        counts_total_dict = dict(zip(keys_total, counts_total))
        normalized_counts = np.array([count / counts_total_dict.get(key, 1) for key, count in zip(keys, counts)])
    else:
        normalized_counts = counts

    if normalize_by_aoi_length:
        normalized_counts = np.array([count / aoi_avg_lengths.get(key, 1) for key, count in zip(keys, normalized_counts)])

    if not sort_first:
        sorted_indices = np.argsort(normalized_counts)
        if top:
            sorted_indices = sorted_indices[::-1]
        keys = keys[sorted_indices]
        counts = counts[sorted_indices]
        normalized_counts = normalized_counts[sorted_indices]

    filtered_indices = [i for i, count in enumerate(counts) if count > fixation_treshold]
    plot_keys = keys[filtered_indices][:n]
    plot_counts = normalized_counts[filtered_indices][:n]

    colors = ['red' if mark_multiline and aoi_multiline[key] else 'blue' for key in plot_keys]

    plt.figure()
    plt.bar(plot_keys, plot_counts, color=colors)
    plt.xlabel('AOI Type')
    plt.ylabel('Normalized Fixation Count' if normalize_by_aoi_occurance or normalize_by_aoi_length else 'Fixation Count')
    plt.title(f'{title}')

def populate_patches_and_diffs(json_line, file_name, file_patches, diff_lines):
    if json_line.get('Response') is not None and isinstance(json_line['Response'], list):
        file_patch = {}
        for item in json_line['Response']:
            if 'patch' in item:
                code_filename = item.get('filename', 'Unknown').replace('.', '').replace('/', '').replace('-', '')

                if code_filename + '-src' not in file_patch:
                    file_patch[code_filename + '-src'] = []

                if code_filename + '-target' not in file_patch:
                    file_patch[code_filename + '-target'] = []

                for patch in item['patch']:
                    startSrcLine = patch.get('startSrcLine')
                    srcLinesLength = patch.get('srcLinesLength')
                    startTargetLine = patch.get('startTagretLine', patch.get('startTargetLine'))
                    targetLinesLength = patch.get('targetLinesLength')

                    file_patch[code_filename + '-src'].append({
                        'start': startSrcLine,
                        'end': startSrcLine + srcLinesLength 
                    })

                    file_patch[code_filename + '-target'].append({
                        'start': startTargetLine,
                        'end': startTargetLine + targetLinesLength 
                    })

                    diff_lines[code_filename + '-target'] = get_diff_lines_for_patch(patch, True)
                    diff_lines[code_filename + '-src'] = get_diff_lines_for_patch(patch, False)
                    
        file_patches[file_name] = file_patch

#TODO need to fix the matrix, it doesn't look like it's supposed to
### gets the adjacency matrix for the fixations
def adjacency_matrix(fixation_list, unique_list):
    
    adj_mtrx = np.zeros((len(unique_list),len(unique_list)))
    for i in range(len(fixation_list)-1):
        if fixation_list[i]!=fixation_list[i+1]:
            adj_mtrx[unique_list.index(fixation_list[i+1])][unique_list.index(fixation_list[i])]=1
    return adj_mtrx
    
### converts the adjacency matrix to markov matrix
def markov_matrix(adj_mtrx):
    markov_mtrx = np.zeros((len(adj_mtrx),len(adj_mtrx)))
    col_sum=0
    for i in range(len(adj_mtrx)):
        for j in range(len(adj_mtrx)):
            col_sum += adj_mtrx[j][i]
        if col_sum!=0:
            for k in range(len(adj_mtrx)):
                markov_mtrx[k][i]=adj_mtrx[k][i]/col_sum
        else:
            for k in range(len(adj_mtrx)):
                markov_mtrx[k][i]=1/len(adj_mtrx)
    
    
    
    return np.mat(markov_mtrx)

### calculates the AOI ranking based on the markov matrix
### each unique AOI is a node in the graph
def AOI_ranking(fixations):
    for key in fixations:
        unique_list = []
        fixation_list = []
        for f in fixations[key]:
            aoi_type= f.get('element-id')
            if not aoi_type in unique_list:
                unique_list.append(aoi_type)
            fixation_list.append(aoi_type)
        markov_mtrx=markov_matrix(adjacency_matrix(fixation_list, unique_list))
        google_mtrx = 0.85*markov_mtrx + 0.15/len(markov_mtrx)
        random_vec1=np.random.rand(len(markov_mtrx))
        random_vec2=np.random.rand(len(markov_mtrx))
        mtrx = np.power(google_mtrx, 10)
        out_vec1 = np.matmul(mtrx, random_vec1)
        out_vec2 = np.matmul(mtrx, random_vec2)
        
        if  np.argmax(out_vec1)== np.argmax(out_vec2):
            print('Converged')
            print(f'for log file: {key}')
            print(f'Ranking based on Markov Matrix:')
            l=5 if len(unique_list)>5 else len(unique_list)
            for i in range(l):
                idx= np.argmax(out_vec1)
                print(f'{unique_list[idx]}') 
                out_vec1[0,idx]=0
        else:
            print('Not Converged')
            return

### calculates the levenshtein distance between the fixations between particiapants of the same PR
def levensthein_distance(fixations):
    all_fixations = {}
    all_distances = {}
    for key in fixations:
        
        fixation_list = []
        for f in fixations[key]:
            aoi_type= f.get('element-id')
            fixation_list.append(aoi_type)
        all_fixations[key] = fixation_list
    keys = list(all_fixations.keys())
    for i in range(len(keys)):
        for j in range(i+1,len(keys)):
            if keys[i][-1]==keys[j][-1]: #if name of files is partxpry, where y is the number of PR
                levenshtein_matrix = np.zeros([len(all_fixations[keys[i]])+1,len(all_fixations[keys[j]])+1])
                for k in range(len(all_fixations[keys[i]])+1):
                    levenshtein_matrix[k][0]=k
                for l in range(len(all_fixations[keys[j]])+1):
                    levenshtein_matrix[0][l]=l
                for k in range(1, len(all_fixations[keys[i]])+1):
                    for l in range(1, len(all_fixations[keys[j]])+1):
                        if all_fixations[keys[i]][k-1]==all_fixations[keys[j]][l-1]:
                            levenshtein_matrix[k][l]=levenshtein_matrix[k-1][l-1]
                        else:
                            levenshtein_matrix[k][l]=min(levenshtein_matrix[k-1][l]+1, levenshtein_matrix[k][l-1]+1, levenshtein_matrix[k-1][l-1]+1)
                print(f'for log files: {keys[i]} and {keys[j]}')
                print('Levenshtein distance:')
                print(levenshtein_matrix[len(all_fixations[keys[i]])][len(all_fixations[keys[j]])])
                print('\n\n')
                all_distances[keys[i]+'-'+keys[j]] = levenshtein_matrix[len(all_fixations[keys[i]])][len(all_fixations[keys[j]])]
    sorted_distances = sorted(all_distances.items(), key=lambda x: x[1])
    print('Sorted distances:')
    for i in range(len(sorted_distances)):
        print(f'{sorted_distances[i][0]}: {sorted_distances[i][1]}')

        
### calculates the NW distance between the fixations between particiapants of the same PR
def NW_distance(fixations):
    all_fixations = {}
    all_distances = {}
    #scoring scheme - simple
    match=1
    mismatch=-1
    gap=-1
    for key in fixations:
        
        fixation_list = []
        for f in fixations[key]:
            aoi_type= f.get('element-id')
            fixation_list.append(aoi_type)
        all_fixations[key] = fixation_list
    keys = list(all_fixations.keys())
    for i in range(len(keys)):
        for j in range(i+1,len(keys)):
            if keys[i][-1]==keys[j][-1]: #if name of files is partxpry, where y is the number of PR
                NW_matrix = np.zeros([len(all_fixations[keys[i]])+1,len(all_fixations[keys[j]])+1])
                for k in range(len(all_fixations[keys[i]])+1):
                    NW_matrix[k][0]=k*gap
                for l in range(len(all_fixations[keys[j]])+1):
                    NW_matrix[0][l]=l*gap
                for k in range(1, len(all_fixations[keys[i]])+1):
                    for l in range(1, len(all_fixations[keys[j]])+1):
                        if all_fixations[keys[i]][k-1]==all_fixations[keys[j]][l-1]:
                            score=match
                        else:
                            a=all_fixations[keys[i]][k-1].split(':')[1].split('-')[-1]
                            b=all_fixations[keys[j]][l-1].split(':')[1].split('-')[-1]
                            
                            if a==b:
                                score=match
                            else:
                                score=-np.log(abs(int(a)-int(b)))
                                
                            # could have this score depend on location of the AOIs in the file
                        NW_matrix[k][l]=max(NW_matrix[k-1][l]+gap, NW_matrix[k][l-1]+gap, NW_matrix[k-1][l-1]+score)
                score=0
                x=len(all_fixations[keys[i]])
                y=len(all_fixations[keys[j]])
                while x>0 and y>0:
                    a=all_fixations[keys[i]][x-1].split(':')[1].split('-')[-1]
                    b=all_fixations[keys[j]][y-1].split(':')[1].split('-')[-1]
                            
                    if a==b:
                        mismatch=match
                    else:
                        mismatch=-np.log(abs(int(a)-int(b)))
                    
                    if all_fixations[keys[i]][x-1]==all_fixations[keys[j]][y-1]:
                        score+=match
                        x-=1
                        y-=1
                    elif NW_matrix[x][y]==NW_matrix[x-1][y-1]+mismatch:
                        score+=mismatch
                        x-=1
                        y-=1
                    elif NW_matrix[x][y]==NW_matrix[x-1][y]+gap:
                        score+=gap
                        x-=1
                    else:
                        score+=gap
                        y-=1
                while x>0:
                    score+=gap
                    x-=1
                while y>0:
                    score+=gap
                    y-=1
                print(f'for log files: {keys[i]} and {keys[j]}')
                print('Needleman-Wunsch distance:')
                print(score)
                all_distances[keys[i]+'-'+keys[j]] = score
    sorted_distances = sorted(all_distances.items(), key=lambda x: x[1])
    print('Sorted distances:')
    for i in range(len(sorted_distances)): 
        print(f'{sorted_distances[i][0]}: {sorted_distances[i][1]}')
                
def save_data_to_file(data, new = False):
    append_argument = 'w' if new else 'a'

    with h5py.File('data.h5', append_argument) as hf:
        for key in data:
            hf.create_dataset(f'{key}', data=data[key])

# Populates useful maps from the log files
def populate_maps(files, fixations, show_more_events, parser_response, parser_response_full, file_patches, diff_lines):
    for f in files:
        file_name = str(f).split(os.sep)[-1]
        log = open(f, 'r', encoding='utf-8')
        lines = log.readlines()
         
        for line in lines:
            try:
                json_line = json.loads(line)
            except (json.decoder.JSONDecodeError) as e:
                continue


            if json_line.get('action') == 'fixation':
                if file_name in fixations:
                    fixations[file_name].append(json_line)
                else:   
                    fixations[file_name] = [json_line]
            elif json_line.get('action') == 'click' and json_line.get('element-id').startswith('show-more-btn'):
                if file_name in show_more_events:
                    show_more_events[file_name].append(json_line)
                else:
                    show_more_events[file_name] = [json_line]
            elif json_line.get('Request') != None and 'parser/parse' in json_line.get('Request'):
                data = json_line.get('Data')
                data_dict = json.loads(data) if isinstance(data, str) else data

                if data_dict.get('full') is True:
                    if file_name in parser_response_full:
                        parser_response_full[file_name].append(json_line)
                    else:
                        parser_response_full[file_name] = [json_line]
                else:
                    if file_name in parser_response:
                        parser_response[file_name].append(json_line)
                    else:
                        parser_response[file_name] = [json_line]
            elif json_line.get('Request') != None and 'parsejavafull' in json_line.get('Request'): # This is for backwards-compatability with older logs
                if file_name in parser_response_full:
                    parser_response_full[file_name].append(json_line)
                else:
                    parser_response_full[file_name] = [json_line]
            elif json_line.get('Request') != None and 'parsejava' in json_line.get('Request'): # This is for backwards-compatability with older logs
                if file_name in parser_response:
                    parser_response[file_name].append(json_line)
                else:
                    parser_response[file_name] = [json_line]
            elif json_line.get('Response') != None and isinstance(json_line['Response'], list):
                populate_patches_and_diffs(json_line, file_name, file_patches, diff_lines)

        log.close()

if __name__ == '__main__':
    
    cwd = os.path.abspath(os.getcwd())
    cwd = os.path.split(cwd)[0]
    cwd = os.path.join(cwd, 'analysis')
    cwd = os.path.join(cwd, '../logs')

    path = Path(cwd)

    files = path.glob('*')

    # Maps with log file as key
    fixations = {}
    show_more_events = {}
    parser_response = {}
    parser_response_full = {}
    file_patches = {}
    diff_lines = {}

    populate_maps(files, fixations, show_more_events, parser_response, parser_response_full, file_patches, diff_lines)

    save_data_to_file(code_base_metrics(parser_response), new=True)
    save_data_to_file(src_vs_target(fixations))
    save_data_to_file(save_visibility_periods(file_patches, show_more_events))
    save_data_to_file(loc_over_time(fixations, show_more_events, file_patches, get_file_lengths(parser_response)))
    save_data_to_file(aoi_over_time(fixations))
    save_data_to_file(code_coverage(fixations, parser_response, diff_lines))
    save_data_to_file(code_coverage_visibility(fixations, parser_response, diff_lines, file_patches, show_more_events))
    save_data_to_file(line_coverage(fixations, parser_response))
    save_data_to_file(code_coverage_over_time(fixations, parser_response, diff_lines, file_patches, show_more_events))

    # loc_over_time_shaded(fixations, show_more_events, file_patches, get_file_lengths(parser_response))
    # cumulative_fixations_per_aoi(fixations)
    # cumulative_fixations_per_aoi(fixations, parser_response, get_visibility_periods(file_patches, show_more_events))
    # aoi_scarf_plot(fixations)
    # code_coverage(fixations, parser_response)
    # AOI_ranking(fixations)
    # levensthein_distance(fixations)
    # NW_distance(fixations)
