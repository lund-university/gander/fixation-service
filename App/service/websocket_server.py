#!/usr/bin/env python

from gazeEventsDetection import gazeEvents, GazeDataReader
import asyncio
import websockets
import service.et_connection as et
import json

tracker = None

async def wsServer(et_tracker):
    global tracker
    tracker = et_tracker
    async with websockets.serve(message_handler, "localhost", 3005):
            await asyncio.Future()   # run forever


async def recv(websocket, mess):
    while True:
        mess.append(await websocket.recv())

async def message_handler(websocket):
    while True: #Wait for client to connect
        mess = await websocket.recv()
        if mess=='{"action": "connect"}':
            break
    #Have to initialize tracker here, otherwise it will not work to loop several times
    tracker.init()
    tracker.start_recording(gaze=True)

    fsm = gazeEvents.GazeEvents()       #Create gazeEvents object to feed real time fixation algortihm
    dataReader = GazeDataReader.GazeDataReader()    #Create GazeDataReader object to convert tobii-sample to Sample
    loop = asyncio.get_event_loop()
    counter = 0         #For debugging, we want counter to be equal to the number of samples comsumed
    received = None     #When polling we want to avoid using the same sample twice
    b = True            #While loop condition
    prev={}
    mess=[]
    prevMess = None
    message_counter=0
    asyncio.ensure_future(recv(websocket, mess))
   # asyncio.async(recv(websocket, mess.append))
    while b:
        try:
            if len(mess) > 0 and mess[-1] != prevMess:
                current_message = mess[-1]

                try:
                    message_data = json.loads(current_message)

                    if prevMess:
                        prevMess_data = json.loads(prevMess)
                except json.JSONDecodeError:
                    print(f"Warning: Could not parse message: {current_message}")
                    prevMess = current_message
                    return  

                message_action = message_data.get("action", "")

                if message_action in {"enteringPRList", "enteringChanges"} and prevMess:
                    tracker.send_message('offset_'+prevMess_data.get("data"))
                    #print('offset_'+prevMess_data.get("data"))
                elif message_action in {"enteringOverview", "enteringFile"} and prevMess:
                    tracker.send_message('offset_'+prevMess_data.get("data"))
                    #print('offset_'+prevMess_data.get("data"))
                    tracker.send_message('onset_'+message_data.get("data"))
                    #print('onset_'+message_data.get("data"))
                elif prevMess:     
                    PR = message_data.get("data")
                    tracker.send_message('onset_'+PR)
                    #print('onset_'+PR)

                prevMess = current_message
                    
            #mess = await websocket.recv()
            
            
                #tracker.send_message(‘’.join('onset_',' NamnPåNåtBra')) nåt liknande, o sen med offset om vi ej är på första gången
                #continue ##need to check if this is blocking, the await shouldn't be, but the if might be, can't be arsed today
                
            received = et.get_next_sample(tracker, prev) #Poll for new sample, only return if new sample is found
            prev=received   #update previous sample

            sample = dataReader.tobii2Sample(received) #convert found sample to Sample object
            
            gaze_state = await loop.run_in_executor(None, fsm_handler, fsm, sample) #Send sample through gazeEvents and get state
            counter+=1
            #print('{"x":"'+str(fsm.x)+'", "y":"'+str(fsm.y)+'", "ts":"'+str(round(fsm.t*1000000))+'"}')
            if gaze_state ==str(0): #true only if gazeEvents is in fixation state
                await websocket.send('{"x":"'+str(fsm.x)+'", "y":"'+str(fsm.y)+'", "ts":"'+str(round(fsm.t*1000000))+'"}') #send to web client
        except Exception as e:
            print(f'\nERROR: {e},  {type(e)}\n')
            b = False
    print(f'counter: {counter}')
    #print(prevMess)
    if 'enterPR' in prevMess or 'enterChange' in prevMess or 'enteringOverview' in prevMess or 'enteringFile' in prevMess:
        tracker.send_message('offset_'+prevMess.split('"')[-2])
    et.close_tracker(tracker)   #Close tracker and save data

def fsm_handler(fsm, sample):
    fsm.addSample(sample.tstamp, sample.left.x, sample.left.y, sample.left.pupil, sample.right.x, sample.right.y, sample.right.pupil)
     #Send Sample through gazeEvents
            
    #Check state of gazeEvents
    return f'{fsm.current_state.type}'

                

       
      


        



    

    
 