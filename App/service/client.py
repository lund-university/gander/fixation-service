import sys
import os
# caution: path[0] is reserved for script path (or '' in REPL)
cwd = os.path.abspath(os.getcwd())
cwd = os.path.split(cwd)[0]
cwd = os.path.join(cwd, 'gazeEventsDetection')
sys.path.insert(1, cwd)
#above is temporary, this should be changed
import websocket, ssl
#import gazeEvents, GazeDataReader
import time


#def on_message(ws, message): 
#    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws, close_status_code, close_msg):
    print("### Closed ###")

def on_open(ws):
    print("Opened connection")


def startConnection():
    ### Create connection to the ws-server. We return the ws instance and should be able to fetch data continously 
    websocket.enableTrace(True)
    ws = websocket.create_connection("ws://localhost:3003", sslopt={"cert_reqs": ssl.CERT_NONE})
    ws.send('{"action":"connect"}')
    recvMessage(ws)
    ws.send('{"action":"setBaseSampleFreq", "freq":90}')
    recvMessage(ws)
    ws.send('{"action":"setSampleStreamFreq", "freq":90}')
    recvMessage(ws)
    #{"action":"startSampleBuffer"}
    ws.send('{"action":"startSampleBuffer"}')
    recvMessage(ws)
    ws.send('{"action":"startSampleStream"}')
    recvMessage(ws)
    return ws


def recvMessage(ws):
    print(ws.recv())

#if __name__ == "__main__":
 
    #gazeEvents = gazeEvents.GazeEvents()
    #dataReader = GazeDataReader.GazeDataReader()
    #
    ##Establish connection to titta websocket
    #ws = startConnection()
    #count = 0
    ##output file
    #f = open("C:\\Users\\fe7865ap\\Jobb\\pygmy\\inloopfixation-service\\gazeEventsDetection\\output.tsv", 'w')
    #tstart = time.time()
    #while count < 90:
    #    #While connected, fetch data and send through algorithm
    #    #rawData = ws.recv()
    #    received = ws.recv()
    #    #Create a new Sample from rawData
    #    sample = dataReader.json2Sample(received)
    #    #Send Sample through gazeEvents
    #    gazeEvents.addSample(sample.tstamp, sample.left.x, sample.left.y, sample.left.pupil, sample.right.x, sample.right.y, sample.right.pupil)
    #    #Check state of gazeEvents
    #    tmp = f'{gazeEvents.current_state.type}\n'
    #    f.write(tmp)
    #    #if state == fixation
    #        #Add sample to list / ping through ws-server that a fixation is detected.
    #    #else -> just pass through and receive next sample from Titta websocket.
    #    count += 1
    #tend = time.time()
    #ws.close()
    #f.close()
    #print(f'Elapsed time: {(tend-tstart)*10**3} ms')

###Wrong format but worth keeping
#         rawData = "{'device_time_stamp': 218434873,'system_time_stamp': 598014069039,'left_gaze_point_available': 1,'left_gaze_point_valid': 1,'left_gaze_point_on_display_area_x': 0.226116,'left_gaze_point_on_display_area_y': -0.0481912,'left_gaze_point_in_user_coordinates_x': -140.707,'left_gaze_point_in_user_coordinates_y': 334.035,'left_gaze_point_in_user_coordinates_z': 113.533,'left_gaze_origin_available': 1,'left_gaze_origin_valid': 1,'left_gaze_origin_in_trackbox_coordinates_x': 0.609613,'left_gaze_origin_in_trackbox_coordinates_y': 0.233797,'left_gaze_origin_in_trackbox_coordinates_z': 0.656354,'left_gaze_origin_in_user_coordinates_x': -57.2532,'left_gaze_origin_in_user_coordinates_y': 107.91,'left_gaze_origin_in_user_coordinates_z': 717.857,'left_pupil_available': 1,'left_pupil_valid': 1,'left_pupil_diameter': 3.11246,'left_eye_openness_available': 0,'left_eye_openness_valid': 0,'left_eye_openness_diameter': nan,'right_gaze_point_available': 1,'right_gaze_point_valid': 1,'right_gaze_point_on_display_area_x': 0.214794,'right_gaze_point_on_display_area_y': -0.069436,'right_gaze_point_in_user_coordinates_x': -146.54,'right_gaze_point_in_user_coordinates_y': 340.464,'right_gaze_point_in_user_coordinates_z': 115.873,'right_gaze_origin_available': 1,'right_gaze_origin_valid': 1,'right_gaze_origin_in_trackbox_coordinates_x': 0.493576,'right_gaze_origin_in_trackbox_coordinates_y': 0.214401,'right_gaze_origin_in_trackbox_coordinates_z': 0.661866,'right_gaze_origin_in_user_coordinates_x': 1.20674,'right_gaze_origin_in_user_coordinates_y': 116.599,'right_gaze_origin_in_user_coordinates_z': 721.572,'right_pupil_available': 1,'right_pupil_valid': 1,'right_pupil_diameter': 3.28825,'right_eye_openness_available': 0,'right_eye_openness_valid': 0,'right_eye_openness_diameter': nan}"    

    
#{"action":"connect"}
#{"action":"setBaseSampleFreq", "freq":600}
#{"action":"setSampleStreamFreq", "freq":15}
#{"action":"startSampleBuffer"}
#{"action":"startSampleStream"}
#{"action":"stopSampleStream"}
#{"action":"stopSampleBuffer"}
#{"action":"saveData","filePath":"C:\\Users\\Administrator\\Desktop\\test.tsv"}
