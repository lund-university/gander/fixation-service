# -*- coding: utf-8 -*-
"""
Created on Mon Sep  4 09:36:50 2023

@author: Marcus
"""
import sys
import os
from pathlib import Path
from datetime import datetime

# caution: path[0] is reserved for script path (or '' in REPL)


# Import relevant modules
from psychopy import visual, core
from titta import Titta

# Create an instance of titta
def init_tracker():
    et_name = 'IS4_Large_Peripheral'
    settings = Titta.get_defaults(et_name)
    path = Path.cwd() / 'sessionData'
    Path(path).mkdir(parents=True, exist_ok=True)

    # Connect to eye tracker and calibrate
    tracker = Titta.Connect(settings)
    tracker.init()
    tracker.start_recording(gaze=True)    
    # # # Window set-up (this color will be used for calibration)
    # win = visual.Window(size=(1920, 1200)) #laptop monitor 
    # #work monitor size
    # #win = visual.Window(screen=1,size=(1920, 1200)) #for work monitor
    # tracker.calibrate(win)
    # win.close()
    
    return tracker



def get_next_sample(tracker, prev):
    while True:
        sample = tracker.buffer.peek_N('gaze',1)
        if len(prev) == 0 and sample['system_time_stamp'].size != 0:
            print(sample)
            return sample
        if sample != prev and sample != {} and sample != None and sample['system_time_stamp'].size != 0 and sample['system_time_stamp'] != prev['system_time_stamp']:
            return sample

######################## Measuring system time #####################################
    # time.sleep(0.3)
    # peek = tracker.buffer.peek_N('gaze', 1)['system_time_stamp']
    # print(peek)
    # tracker_sys_time = tracker.get_system_time_stamp()
    # print(tracker_sys_time)
    # print(f'delay: {tracker_sys_time - int(peek[0])}')



# Show your stimuli
#win.flip()
#core.wait(1)

def close_tracker(tracker):
    tracker.stop_recording(gaze=True)
    
    # print(f'buffer length: {len(tracker.buffer.consume_N("gaze")["system_time_stamp"])}')

    # Stop recording
    print("stop recording" )
    print(sys.path[1])
    now = datetime.now()
    format = '%Y%m%d%I_%M_%S'
    #TODO continue with file formatting 
    cwd = os.path.abspath(os.getcwd())
    cwd = os.path.split(cwd)[0]
    cwd = os.path.join(cwd, 'App')
    cwd = os.path.join(cwd, 'sessionData')
    cwd = os.path.join(cwd, now.strftime(format))
    sys.path.insert(1, cwd)
    #  save data, 
    tracker.save_data(filename=sys.path[1])
