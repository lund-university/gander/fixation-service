In Loop Fixation Service

We are using the gaze_movements_FSM, which can be found at https://bitbucket.org/diaztula/gaze_movements_fsm/src/master/ 

We have used python 2to3 to convert their source code to python3 and use various modules (numpy etc) which can be easily downloaded via pip.

Modules needed to run this project:
numpy
websockets

The algorithm is meant to be run in parallell with our Gander Client to detect fixation points in the loop while users are reviewing code.
It can handle input of different formats. We are focused on the tsv format right now but might integrate support for other formatsw as developement goes on.

Will update this README as time goes on.