FROM python:3.9-slim-buster
# Or any preferred Python version.
RUN mkdir -p /opt/fixation-service 
#################dependencies###################

RUN pip install numpy 
RUN pip install websocket-client
RUN pip install websockets
RUN pip install asyncio
RUN pip install scipy
RUN pip install matplotlib 

WORKDIR /opt/fixation-service
RUN mkdir -p /App

COPY App ./App


##################
#having problems with the workdir, I want it to copy both /App folder and /gazeevents detection but run in app
###################



CMD ["python3", "App/main.py"] 