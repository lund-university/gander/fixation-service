from math import atan2, degrees
h = 19           # Monitor height in cm
d = 52           # Distance between monitor and participant in cm
r = 1200          # Vertical resolution of the monitor
size_in_deg_acc = 0.2 # The stimulus size in pixels
size_prec=0.05
# Calculate the number of degrees that correspond to a single pixel. This will
# generally be a very small value, something like 0.03.
deg_per_px_acc = degrees(atan2(.5 * h, d)) / (.5 * r)
print(f'{deg_per_px_acc} degrees correspond to a single pixel')
# Calculate the size of the stimulus in degrees
size_in_px = size_in_deg_acc / deg_per_px_acc
print(f'The accuracy is {size_in_px} pixels and {size_in_deg_acc} visual degrees')

deg_per_px_prec = degrees(atan2(.5 * h, d)) / (.5 * r)
print(f'{deg_per_px_acc} degrees correspond to a single pixel')
# Calculate the size of the stimulus in degrees
size_in_px = size_prec / deg_per_px_acc
print(f'The precision is {size_in_px} pixels and {size_prec} visual degrees')